import { Component, OnInit, Renderer2, ViewChild, ElementRef, Directive } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
import { ROUTES } from '../sidebar/sidebar.component';
import { AuthenticationService } from 'providers/authentication.service';
import { ConfigService } from 'providers/config/config.service';
import * as moment from 'moment';

var misc: any = {
    navbar_menu_visible: 0,
    active_collapse: true,
    disabled_collapse_init: 0,
}

export const imagesArray: string[] = [
    'kirill-tonkikh-jM-_P7EqaQk-unsplash',
    'ronaldo-de-oliveira-zAkISnvAHRo-unsplash',
    'randy-fath-ymf4_9Y9S_A-unsplash',
    'tim-gouw--gjHizUfFlM-unsplash',
    'kristopher-roller-rgrZVTjuuPw-unsplash',
    'jukan-tateisi-bJhT_8nbUA0-unsplash'
  ];
export class ChallengeStatsVm {
    healthPersonal: number = 0;
    healthProfessional: number = 0;
    healthGroupScore: number = 0;
    objAndKeyResultsMetric1: number = 0;
    objAndKeyResultsMetric2: number = 0;
    objAndKeyResultsMetric3: number = 0;
    objAndKeyResultsMetric4: number = 0;
  }
  
  export class CHALLENGE {
    id: string = '';
    name: string = '';
    abstract: string = '';
    overview?: string = '';
    createdAt: string = '';
    publishedAt: string = '';
    deadlineISO?: Date = null;
    deadline: string = '';
    //company: string = '';
    creator: string = '';
    creatorName: string = '';
    //category: string = '';
    video?: string = '';
    category_id?: string = '';
    image: string = '';
    membersTotal?: number = 0;
    active: boolean = false;
    challengeStats?: ChallengeStatsVm = new ChallengeStatsVm();
  }


  

@Component({
    moduleId: module.id,
    selector: 'navbar-cmp',
    templateUrl: 'navbar.component.html',
    styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {
    private listTitles: any[];
    location: Location;
    private nativeElement: Node;
    private toggleButton;
    private sidebarVisible: boolean;
    private _router: Subscription;
    public open: boolean = false;

    @ViewChild("navbar-cmp", { static: false }) button;

    private subscriptionUser: Subscription;

    allChallengesRecords: any;
    public challengesRecords: CHALLENGE[] = [];
    public challengesRecordsTemp: CHALLENGE[] = [];
    membersTotal: number = 0;
    token: string = '';
    loading: boolean = true;
    user = {
        id: '',
        publicid: '',
        token: '',
        role: '',
        fullName: '',
        name: '',
        email: '',
        avatar: '',
        emailVerification: null
    };

    constructor(
        location: Location,
        private renderer: Renderer2,
        private element: ElementRef,
        private router: Router,
        private _authService: AuthenticationService,
        private readonly _configService: ConfigService
    ) {
        this.location = location;
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }

    async ngOnInit() {

        this.listTitles = ROUTES.filter(listTitle => listTitle);

        const navbar: HTMLElement = this.element.nativeElement;
        const body = document.getElementsByTagName('body')[0];
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];

        this._router = this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: NavigationEnd) => {
            const $layer = document.getElementsByClassName('close-layer')[0];
            if ($layer) {
                $layer.remove();
            }
        });

        this.subscriptionUser = this._authService.authUserChange.subscribe(async value => {
            let user: any[] = value ? JSON.parse(value) : {}
            this.user.id = user['_id'];
            this.user.token = user['token'];
            this.user.name = user['name'];
            this.user.fullName = user['name'] + ' ' + user['surname'];
            this.user.email = user['email'];
            this.user.role = user['role'];
            this.user.publicid = user['publicid'];
            this.user.emailVerification = user['emailVerification'];

            // if (this.user.emailVerification) {
            //     this.privilege = user['privilege'];
            //     if (user['role'] == 'Admin') this.admin = true;
            //     else this.admin = false;
            // }

            // this.showUnverifiedEmail = !this.user.emailVerification;
            this.user.avatar = this._configService.url + user['avatar'];
        });
        await this.getAllChallenges();
        console.log(this.subscriptionUser);
        
    }

    ngOnDestroy() {
        this.subscriptionUser.unsubscribe();
    }

    ngAfterViewInit() {
        //this.minimizeSidebar();
    }

    backButton() {
        this.location.back();
    }

    async logout() {
        await this._authService.logout();
        this.router.navigate(['/login']);
    }


/*     minimizeSidebar() {
        const body = document.getElementsByTagName('body')[0];

        if (misc.sidebar_mini_active === true) {
            body.classList.remove('sidebar-mini');
            misc.sidebar_mini_active = false;

        } else {
            setTimeout(function () {
                body.classList.add('sidebar-mini');

                misc.sidebar_mini_active = true;
            }, 300);
        }

        // we simulate the window Resize so the charts will get updated in realtime.
        const simulateWindowResize = setInterval(function () {
            window.dispatchEvent(new Event('resize'));
        }, 180);

        // we stop the simulation of Window Resize after the animations are completed
        setTimeout(function () {
            clearInterval(simulateWindowResize);
        }, 1000);
    } */

    isMobileMenu() {
        if (window.outerWidth < 991) {
            return false;
        }
        return true;
    }

/*     sidebarOpen() {
        var toggleButton = this.toggleButton;
        var html = document.getElementsByTagName('html')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        const mainPanel = <HTMLElement>document.getElementsByClassName('main-panel')[0];
        if (window.innerWidth < 991) {
            mainPanel.style.position = 'fixed';
        }
        html.classList.add('nav-open');
        this.sidebarVisible = true;
    }

    sidebarClose() {
        var html = document.getElementsByTagName('html')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        html.classList.remove('nav-open');
        const mainPanel = <HTMLElement>document.getElementsByClassName('main-panel')[0];

        if (window.innerWidth < 991) {
            setTimeout(function () {
                mainPanel.style.position = '';
            }, 500);
        }
    }

    sidebarToggle() {
        // var toggleButton = this.toggleButton;
        // var body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible == false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    } */

    getTitle() {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(1);
        }
        for (var item = 0; item < this.listTitles.length; item++) {
            var parent = this.listTitles[item];
            if (parent.path === titlee) {
                return parent.title;
            } else if (parent.children) {
                // var children_from_url = titlee.split("/")[2];
                var children_from_url = titlee.split("/")[1];
                for (var current = 0; current < parent.children.length; current++) {
                    if (parent.children[current].path === children_from_url) {
                        return parent.children[current].title;
                    }
                }
            }
        }
        return 'Dashboard';
    }

    getPath() {
        // console.log(this.location);
        return this.location.prepareExternalUrl(this.location.path());
    }

    SearchItems(event) {
        const val = event.target.value.toLowerCase();
        // this.challengesRecordsTemp.forEach(item => Object.keys(item).forEach(k => item[k] = (item[k] == null || item[k] == undefined) ? '' : item[k]));
        const temp = this.challengesRecordsTemp.filter(function (d) {
          return d.name.toString().toLowerCase().indexOf(val) !== -1 ||
            d.abstract.toString().toLowerCase().indexOf(val) !== -1 ||!val;
            //d.category.toString().toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.challengesRecords = temp;
      }

      async getAllChallenges() {

        const challengeVm = {
          id: this.user,
          token: this.token,
        };
    
        this.loading = true;
        console.log(challengeVm);
        
        await (await this._authService.getAllChallenges(challengeVm)).subscribe(newData => {
          //console.log(newData);
          //this.allChallengesRecords = [];
          this.challengesRecords = [];
          this.allChallengesRecords = newData;
          console.log(this.allChallengesRecords);
          
          for (let index = 0; index < this.allChallengesRecords.length; index++) {
            const challenge = this.allChallengesRecords[index];
            this.challengesRecords.push({
              id: challenge._id,
              name: challenge.name,
              createdAt: challenge.createdAt,
              publishedAt: moment(challenge.createdAt).fromNow(),
              abstract: challenge.abstract,
              //company: challenge.company.name,
              //category: challenge.category.name,
              deadline: moment(challenge.deadline).format('Do MMMM YYYY'),
              creator: challenge.creator._id,
              creatorName: challenge.creator.name + ' ' + challenge.creator.surname,
              membersTotal: challenge.members.length,
              active: challenge.active,
              challengeStats: challenge.stats,
              image: 'assets/img/challenges/' + imagesArray[Math.floor(Math.random() * imagesArray.length)] + '.jpg'
            })
          }
    
          this.challengesRecords.sort(function (a, b) {
            if (a.createdAt > b.createdAt) { return -1; }
            if (a.createdAt < b.createdAt) { return 1; }
            return 0;
          });
    
          this.challengesRecordsTemp = this.challengesRecords;
    
          this.membersTotal = this.challengesRecords.reduce(function (n, challenge) {
            return challenge.membersTotal + n;
          }, 0);
    
          this.loading = false;
          console.log(this.challengesRecords);
        }, err => {
          console.log('error');
        });
      }
    

    
      onSelect(data): void {
        console.log('Item clicked', JSON.parse(JSON.stringify(data)));
      }
    

    

    

    

    

}
