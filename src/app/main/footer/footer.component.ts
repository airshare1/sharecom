import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'footer-cmp',
    templateUrl: 'footer.component.html'
})

export class FooterComponent {

    constructor(private location: Location, public router: Router) { }

    today: Date = new Date();

    async backClicked() {
        await this.location.back();
    }
}
