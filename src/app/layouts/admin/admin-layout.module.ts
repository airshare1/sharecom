import { UserComponent } from 'app/pages/app-area/userpage/user.component';
import { CommonModule } from '@angular/common';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgbDatepicker, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { TagInputModule } from 'ngx-chips';
import { DashboardComponent } from 'app/pages/dashboard/dashboard.component';
import { AddReviewComponent } from 'app/add-review/add-review.component';
import { InformationComponent } from 'app/pages/app-area/information/information.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { AdministrationComponent } from 'app/pages/admin-area/administration/administration.component';
import { AdminUserComponent } from 'app/pages/admin-area/admin-user/admin-user.component';
import { AvatarModule } from 'ngx-avatar';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import interactionPlugin from '@fullcalendar/interaction'; // a plugin!
import timeGridPlugin from '@fullcalendar/timegrid';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { UploadFileComponent } from '../../pages/multimedia-area/upload-file/upload-file.component';
import { PostsComponent } from 'app/posts/posts.component';

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  timeGridPlugin,
  interactionPlugin
]);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    // NgbDatepicker,
    JwBootstrapSwitchNg2Module,
    NgMultiSelectDropDownModule.forRoot(),
    NgxDatatableModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    RouterModule.forChild(AdminLayoutRoutes),
    TagInputModule,
    AvatarModule,
    NgxDnDModule,
    NgxChartsModule,
    FullCalendarModule // register FullCalendar with you app
    // NgxDnDModule.forRoot()
  ],
  declarations: [
    UserComponent,
    InformationComponent,
    AdministrationComponent,
    AdminUserComponent,
    UploadFileComponent,
    AddReviewComponent,
    PostsComponent
  ],
  entryComponents: [
    UploadFileComponent
  ]
})

export class AdminLayoutModule { }