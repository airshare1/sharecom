import { Routes } from '@angular/router';
import { DashboardComponent } from 'app/pages/dashboard/dashboard.component';
import { UserComponent } from 'app/pages/app-area/userpage/user.component';
import { InformationComponent } from 'app/pages/app-area/information/information.component';
import { AdministrationComponent } from 'app/pages/admin-area/administration/administration.component';
import { ExtendedFormsComponent } from 'app/forms/extendedforms/extendedforms.component';
import { AdminUserComponent } from 'app/pages/admin-area/admin-user/admin-user.component';
import { ContactAreaComponent } from 'app/pages/contact-area/contact-area.component';
import { CreateChallengeComponent } from 'app/pages/advertisement-area/create-challenge/create-challenge.component';
import { ViewChallengeListComponent } from 'app/pages/advertisement-area/view-challenge-list/view-challenge-list.component';
import { ViewChallengeComponent } from 'app/pages/advertisement-area/view-challenge/view-challenge.component';
import { MembershipComponent } from 'app/pages/payment-area/membership/membership.component';
import { AddReviewComponent } from 'app/add-review/add-review.component';
import { PostsComponent } from 'app/posts/posts.component';

export const AdminLayoutRoutes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'profile', component: UserComponent },
  { path: 'admin', component: AdministrationComponent },
  { path: 'information', component: InformationComponent },
  { path: 'admin-user', component: AdminUserComponent },
  { path: 'contact-area', component: ContactAreaComponent },
   { path: 'create-challenege', component:CreateChallengeComponent },
   { path: 'add-review', component:AddReviewComponent },
   { path: 'add-posts', component:PostsComponent },
   { path: 'challenge-view-list', component: ViewChallengeListComponent },
   { path: 'challenge-view', component: ViewChallengeComponent },
   { path: 'my-stripe-account', component: MembershipComponent },

]

