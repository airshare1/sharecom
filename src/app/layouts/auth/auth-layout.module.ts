import { ContactComponent } from 'app/pages/app-area/contact/contact.component';
import { AboutUsComponent } from 'app/pages/app-area/about-us/about-us.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthLayoutRoutes } from './auth-layout.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from 'app/pages/app-area/login/login.component';
import { RegisterComponent } from 'app/pages/app-area/register/register.component';
import { PasswordResetComponent } from '../../pages/app-area/password-reset/password-reset.component';
import { ForgotPasswordComponent } from '../../pages/app-area/forgot-password/forgot-password.component';
import { DashboardComponent } from 'app/pages/dashboard/dashboard.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AuthLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    AboutUsComponent,
    ContactComponent,
    PasswordResetComponent,
    ForgotPasswordComponent,
    
  ]
})

export class AuthLayoutModule { }
