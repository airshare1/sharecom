import { Routes } from '@angular/router';
import { AboutUsComponent } from 'app/pages/app-area/about-us/about-us.component';
import { ContactComponent } from 'app/pages/app-area/contact/contact.component';
import { ForgotPasswordComponent } from 'app/pages/app-area/forgot-password/forgot-password.component';
import { LoginComponent } from 'app/pages/app-area/login/login.component';
import { PasswordResetComponent } from 'app/pages/app-area/password-reset/password-reset.component';
import { RegisterComponent } from 'app/pages/app-area/register/register.component';

export const AuthLayoutRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'aboutus', component: AboutUsComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'forgot-password', component: ForgotPasswordComponent },
    { path: 'password-reset/:id', component: PasswordResetComponent },
    // { path: 'email-registration/:email', component: RegisterEmailInvitationComponent },
    // { path: 'terms', component: TermsComponent },
    // { path: 'privacy', component: PrivacyComponent },
    // { path: 'email-verification/:email/:code', component: EmailVerificationComponent }
];
