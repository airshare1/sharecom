import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  content: string;
  username: string; // Add the 'username' property
  posts: any[] = [];

  constructor() {}

  ngOnInit() {}

  likes(i) {
    this.posts[i].likes++;
  }

  addComment(i, comment) {
    this.posts[i].comments.push(comment);
  }

  addPost() {
    const obj = {
      'content': this.content,
      'username': this.username, // Include the 'username' property
      'likes': 0,
      'comments': []
    };
    this.posts.push(obj);
  }

}
