import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'providers/authentication.service';
import { regexValidators } from 'providers/validator/validator';
import { NgbDate, NgbCalendar, NgbDateAdapter, NgbDateParserFormatter, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';


declare var $: any;

export const MAXFILES = 100;

/**
 * This Service handles how the date is represented in scripts i.e. ngModel.
 */


/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */


@Component({
  selector: 'app-add-review',
  templateUrl: './add-review.component.html',
  styleUrls: ['./add-review.component.css']
})
export class AddReviewComponent implements OnInit{

  constructor() { }
  @Input() mypost;

  @Output() like = new EventEmitter();
  @Output() ecomment = new EventEmitter();
  comment;
  ngOnInit() {
  }
  mylike(){
    this.like.emit();
  }
  addComment(){
    this.ecomment.emit(this.comment)
  }

  
}