import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { FixedPluginModule } from './shared/fixedplugin/fixedplugin.module';
import { FooterModule } from './main/footer/footer.module';
import { NavbarModule } from './main/navbar/navbar.module';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { AppRoutingModule } from './app.routing';
import { NgcCookieConsentModule, NgcCookieConsentConfig } from 'ngx-cookieconsent';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { TagInputModule } from 'ngx-chips';
import { ConfigService } from 'providers/config/config.service';
import { EventEmitterService } from 'providers/event-emitter/event-emitter.service';
import { AuthenticationService } from 'providers/authentication.service';
import { UserService } from 'providers/user.service';
import { StoreProvider } from 'providers/store/store';
import { RedirectGuard } from 'providers/redirect-guard';
import { NgbModule, NgbDatepicker ,NgbDate,NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import { SidebarModule } from './main/sidebar/sidebar.module';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { PhotoViewer } from "@awesome-cordova-plugins/photo-viewer/ngx";
import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { AvatarModule } from 'ngx-avatar';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ContactAreaComponent } from './pages/contact-area/contact-area.component';
import { UploadProfilePictureComponent } from './pages/multimedia-area/upload-profile-picture/upload-profile-picture.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { CreateChallengeComponent } from './pages/advertisement-area/create-challenge/create-challenge.component';
import { ViewChallengeListComponent } from './pages/advertisement-area/view-challenge-list/view-challenge-list.component';
import { ViewChallengeComponent } from './pages/advertisement-area/view-challenge/view-challenge.component';
import { HomepageComponent } from './homepage/homepage.component';
import { MembershipComponent } from './pages/payment-area/membership/membership.component';
import { PostsComponent } from './posts/posts.component';


const cookieConfig: NgcCookieConsentConfig = {
    // Change to true when deploying in cloud. Cookie for specified domain i.e. Template.ie Change to new domain if needed.
    "enabled": false,
    "cookie": {
         "domain": "localhost",
        //"domain": Template add company domain  name here
    },
    "position": "bottom",
    "theme": "classic",
    "palette": {
        "popup": {
            "background": "#6abcfc",
            "text": "#ffffff",
            "link": "#ffffff"
        },
        "button": {
            "background": "#ffffff",
            "text": "#6abcfc",
            "border": "transparent"
        }
    },
    "type": "info",
    "content": {
        "message": "This website stores cookies on your computer. These cookies are used to collect information about how you use our website and to remember you. We use this information to improve and personalize your browsing experience, and for analysis and measurement data about our visitors, both on this website and through other media. See our Privacy Policy for more information about the cookies we use.",
        "dismiss": "Got it!",
        "deny": "Refuse cookies",
        "link": "Learn more",
        "href": "/#/privacy",
        "policy": "Cookie Policy"
    }
};

@NgModule({
    declarations: [
        AppComponent,
        AdminLayoutComponent,
        AuthLayoutComponent,
        ContactAreaComponent,
        UploadProfilePictureComponent,
        DashboardComponent,
        CreateChallengeComponent,
        ViewChallengeListComponent,
        ViewChallengeComponent,
        MembershipComponent,
        
        
    ],
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        CommonModule,
        RouterModule,
        NgbModule,
        NgbDatepickerModule,
        HttpModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        FixedPluginModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,
        BrowserModule,
        HttpClientModule,
        InfiniteScrollModule,
        NgxDnDModule,
        // NgbDatepicker,
        AvatarModule,
        NgxChartsModule,
        // ComponentsModule,
        AppRoutingModule,
        NgxDatatableModule,
        NgcCookieConsentModule.forRoot(cookieConfig),
        NgMultiSelectDropDownModule.forRoot(),
        TagInputModule,
    ],
    providers: [
        ConfigService,
        EventEmitterService,
        AuthenticationService,
        UserService,
        StoreProvider,
        PhotoViewer,
        RedirectGuard
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
