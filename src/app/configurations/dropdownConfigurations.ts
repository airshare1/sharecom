
declare interface USERROLETYPE {
  value: string;
  role: string;
};

export const userRoleType: USERROLETYPE[] = [
  { value: "Admin", role: "System Administrator", },
  { value: "User", role: "User" },
  { value: "Manager", role: "Manager" },
  { value: "CompanyAdmin", role: "Company Administrator" },
];