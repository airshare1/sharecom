import { Component, Input, OnInit } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor(
    private router:Router
  ) { }
  @Input() isShowFilter!: boolean
  isHeaderFilterActive: boolean = false

  ngOnInit(): void {
  }

  toggleHeaderFilter() {
    this.isHeaderFilterActive = !this.isHeaderFilterActive
  }

  LoginPage(){
    this.router.navigate(['/login']);
  }

}
