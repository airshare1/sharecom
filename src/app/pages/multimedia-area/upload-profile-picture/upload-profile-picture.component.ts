import { first } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from 'providers/authentication.service';

declare var $: any;

@Component({
  selector: 'app-upload-profile-picture',
  templateUrl: './upload-profile-picture.component.html',
  styleUrls: ['./upload-profile-picture.component.css']
})
export class UploadProfilePictureComponent implements OnInit {

  @Input() user: string;
  @Input() token: string;
  @Input() avatar: string;

  success = false;
  loading = false;
  submitted = false;
  showError = false;

  errorSize = '';

  uploadform: FormGroup;
  fileToUpload: File = null;

  imgPreview: any = null;

  Toast = Swal.mixin({
    toast: true,
    position: 'bottom',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  constructor(public activeModal: NgbActiveModal,
    private _authService: AuthenticationService,
    private formBuilder: FormBuilder) {
  }

  async ngOnInit() {
    this.uploadform = this.formBuilder.group({
      checked: [false, Validators.requiredTrue],
    });
  }

  fileChangeEvent(fileInput: any) {

    if (!fileInput) {

      this.fileToUpload = null;
      $(".fileinput").fileinput("clear");
      this.imgPreview = null;
      this.errorSize = '';
    } else {
      this.fileToUpload = (<File>fileInput.target.files[0]);
      let reader = new FileReader();
      reader.onload = (fileInput: any) => {
        this.imgPreview = fileInput.target.result;
      }
      reader.readAsDataURL(fileInput.target.files[0]);

      const isTooBig = this.fileToUpload.size > 10 * 1e+6 ? true : false;

      if (!isTooBig) this.errorSize = '';
      else this.errorSize = 'File exceeds the maximum upload size.';
    }

  }

  get f() { return this.uploadform.controls; }

  async onSubmit() {

    this.submitted = true;
    this.showError = false;
    this.success = false;

    // If there was an error in validation
    if (!this.fileToUpload || this.uploadform.invalid) return;

    // Disable the button and show the spinner
    this.loading = true;

    const multimediaVm = {
      id: this.user,
      token: this.token,
    }

    let formData: any = new FormData();

    formData.append('file', this.fileToUpload);
    Object.keys(multimediaVm).forEach(key => formData.append(key, multimediaVm[key]));
    console.log(formData);
    await (await this._authService.uploadProfilePicture(formData))
      .pipe(first()).subscribe(data => {
        this.loading = false;
        this.submitted = false;
        this.success = true;

        setTimeout(() => {
          this.success = false;
        }, 15000);

        setTimeout(() => {
          this.activeModal.close('success')
        }, 3000);

      }, err => {
        this.showError = true;
        let error = 'An error has occurred.'
        if (err.error.message) error = err.error.message;
        this.Toast.fire({
          icon: 'error',
          title: ' Your profile picture has not been uploaded. ' + error + ' Please Try Again.',
        })
        this.loading = false;
        this.submitted = false;
      })
  }
}