


import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from 'providers/authentication.service';
import { ConfigService } from 'providers/config/config.service';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import * as moment from 'moment';

export const imagesArray: string[] = [
  'kirill-tonkikh-jM-_P7EqaQk-unsplash',
  'ronaldo-de-oliveira-zAkISnvAHRo-unsplash',
  'randy-fath-ymf4_9Y9S_A-unsplash',
  'tim-gouw--gjHizUfFlM-unsplash',
  'kristopher-roller-rgrZVTjuuPw-unsplash',
];

export class ChallengeStatsVm {
  healthPersonal: number = 0;
  healthProfessional: number = 0;
  healthGroupScore: number = 0;
  objAndKeyResultsMetric1: number = 0;
  objAndKeyResultsMetric2: number = 0;
  objAndKeyResultsMetric3: number = 0;
  objAndKeyResultsMetric4: number = 0;
}

export class CHALLENGE {
  id: string = '';
  name: string = '';
  abstract: string = '';
  overview?: string = '';
  createdAt: string = '';
  publishedAt: string = '';
  deadlineISO?: Date = null;
  deadline: string = '';
  //company: string = '';
  creator: string = '';
  creatorName: string = '';
  //category: string = '';
  video?: string = '';
  category_id?: string = '';
  image: string = '';
  membersTotal?: number = 0;
  active: boolean = false;
  challengeStats?: ChallengeStatsVm = new ChallengeStatsVm();
}

@Component({
  selector: 'app-view-challenge-list',
  templateUrl: './view-challenge-list.component.html',
  styleUrls: ['./view-challenge-list.component.css']
})
export class ViewChallengeListComponent implements OnInit, OnDestroy {

  user: string = '';
  token: string = '';

  isUserManager: boolean = false;
  loading: boolean = true;

  membersTotal: number = 0;

  allChallengesRecords: any;
  public challengesRecords: CHALLENGE[] = [];
  public challengesRecordsTemp: CHALLENGE[] = [];

  private subscriptionUser: Subscription;

  constructor(
    private _authService: AuthenticationService,
    private readonly _configService: ConfigService,
    private modalService: NgbModal,
    private router: Router
  ) { }

  public async ngOnInit() {

    this.subscriptionUser = this._authService.authUserChange.subscribe(async value => {
      let user: any[] = value ? JSON.parse(value) : {}
      this.user = user['_id'];
      this.token = user['token'];

/*       if (user['role'] == 'Admin' || user['role'] == 'CompanyAdmin' || user['role'] == 'Manager') this.isUserManager = true;
      else this.isUserManager = false; */

    });

    await this.getAllChallenges();
    await this._authService.setChallenge('');

  }

  SearchItems(event) {
    const val = event.target.value.toLowerCase();
    // this.challengesRecordsTemp.forEach(item => Object.keys(item).forEach(k => item[k] = (item[k] == null || item[k] == undefined) ? '' : item[k]));
    const temp = this.challengesRecordsTemp.filter(function (d) {
      return d.name.toString().toLowerCase().indexOf(val) !== -1 ||
        d.abstract.toString().toLowerCase().indexOf(val) !== -1 ||!val;
        //d.category.toString().toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.challengesRecords = temp;
  }

  ngOnDestroy() {
    this.subscriptionUser.unsubscribe();
  }

  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  async getAllChallenges() {

    const challengeVm = {
      id: this.user,
      token: this.token,
    };

    this.loading = true;
    console.log(challengeVm);
    
    await (await this._authService.getAllChallenges(challengeVm)).subscribe(newData => {
      console.log(newData);
      //this.allChallengesRecords = [];
      this.challengesRecords = [];
      this.allChallengesRecords = newData;
      console.log(this.allChallengesRecords);
      
      for (let index = 0; index < this.allChallengesRecords.length; index++) {
        const challenge = this.allChallengesRecords[index];
        this.challengesRecords.push({
          id: challenge._id,
          name: challenge.name,
          createdAt: challenge.createdAt,
          publishedAt: moment(challenge.createdAt).format('Do MMM '),
          abstract: challenge.abstract,
          overview:challenge.overview,
          //company: challenge.company.name,
          //category: challenge.category.name,
          deadline: moment(challenge.deadline).format('Do MMM '),
          creator: challenge.creator._id,
          creatorName: challenge.creator.name + ' ' + challenge.creator.surname,
          membersTotal: challenge.members.length,
          active: challenge.active,
          challengeStats: challenge.stats,
          image: 'assets/img/challenges/' + imagesArray[Math.floor(Math.random() * imagesArray.length)] + '.jpg'
        })
      }

      this.challengesRecords.sort(function (a, b) {
        if (a.createdAt > b.createdAt) { return -1; }
        if (a.createdAt < b.createdAt) { return 1; }
        return 0;
      });

      this.challengesRecordsTemp = this.challengesRecords;

      this.membersTotal = this.challengesRecords.reduce(function (n, challenge) {
        return challenge.membersTotal + n;
      }, 0);

      this.loading = false;
      console.log(this.challengesRecords);
    }, err => {
      console.log('error');
    });
  }

  async editChallenge(challenge: CHALLENGE) {
    if (challenge.creator != this.user || !this.isUserManager) return;

    await this._authService.setChallenge(challenge.id);
    this.router.navigate(['/challenge-edit']);
  }

  async goToChallenge(challenge: CHALLENGE) {
    await this._authService.setChallenge(challenge.id);
    this.router.navigate(['/challenge-view']);
  }

  async deleteChallenge(challenge: CHALLENGE) {

    if (challenge.creator != this.user || !this.isUserManager) return;

    Swal.fire({
      title: 'Delete Challenge. Are you sure?',
      icon: 'warning',
      html: '<div class="form-group">' +
        '<span data-notify="message"> This challenge will be deleted. All the data will be deleted and the challenge cannot be restored.</span>' +
        '<br> <br>' +
        '<label>"' + challenge.name + '"</label></div>',
      showCancelButton: true,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success',
      },
      confirmButtonText: 'Yes, delete it!',
      buttonsStyling: false,
      allowOutsideClick: () => !Swal.isLoading()
    }).then(async (result: any) => {
      if (result.value) {
        const challengeVm = {
          id: this.user,
          token: this.token,
          challenge: challenge.id
        };
        await (await this._authService.deleteChallenge(challengeVm)).subscribe(async data => {
          Swal.fire({
            title: 'Deleted!',
            text: 'Challenge has been deleted.',
            icon: 'success',
            customClass: {
              confirmButton: "btn btn-success",
            },
            buttonsStyling: false
          })
          await this.getAllChallenges();
        }, err => {
          let error = 'An error has occurred.'
          if (err.error.message) error = err.error.message;
          Swal.fire({
            title: 'Failed!',
            text: error + ' Challenge has not been deleted. Please Try Again.',
            icon: 'error',
            customClass: {
              confirmButton: "btn btn-danger",
            },
            buttonsStyling: false
          })
        })
      } else {
        Swal.fire({
          title: 'Cancelled',
          text: 'Challenge has not been deleted.',
          icon: 'error',
          customClass: {
            confirmButton: "btn btn-info",
          },
          buttonsStyling: false
        })
      }
    })
  }

  async activateChallenge(challenge: CHALLENGE) {

    if (challenge.creator != this.user || !this.isUserManager) return;

    if (challenge.active) {
      Swal.fire({
        title: 'Deactivate Challenge. Are you sure?',
        icon: 'warning',
        html: '<div class="form-group">' +
          '<span data-notify="message"> This challenge will be deactivated and its members will no longer have access to it.</span>' +
          '<br> <br>' +
          '<label>"' + challenge.name + '"</label></div>',
        showCancelButton: true,
        customClass: {
          cancelButton: 'btn btn-danger',
          confirmButton: 'btn btn-success',
        },
        confirmButtonText: 'Yes, deactivate it!',
        buttonsStyling: false,
        allowOutsideClick: () => !Swal.isLoading()
      }).then(async (result: any) => {
        if (result.value) {
          let challengeVm = {
            id: this.user,
            token: this.token,
            challenge: challenge.id,
          };
          await (await this._authService.activateChallenge(challengeVm)).subscribe(async data => {
            Swal.fire({
              title: 'Deactivated!',
              text: 'Challenge has been deactivated and its members will not be able to access it.',
              icon: 'success',
              customClass: {
                confirmButton: "btn btn-success",
              },
              buttonsStyling: false
            })
            // await this.getAllChallenges();
            challenge.active = !challenge.active;
          }, err => {
            let error = 'An error has occurred.'
            if (err.error.message) error = err.error.message;
            Swal.fire({
              title: 'Failed!',
              text: error + ' Challenge has not been deactivated. Please Try Again.',
              icon: 'error',
              customClass: {
                confirmButton: "btn btn-danger",
              },
              buttonsStyling: false
            })
          })
        } else {
          Swal.fire({
            title: 'Cancelled',
            text: 'Challenge has not been deactivated.',
            icon: 'error',
            customClass: {
              confirmButton: "btn btn-info",
            },
            buttonsStyling: false
          })
        }
      })
    } else {
      Swal.fire({
        title: 'Activate Challenge. Are you sure?',
        icon: 'warning',
        html: '<div class="form-group">' +
          '<span data-notify="message"> This challenge will be activated and it will have no members.</span>' +
          '<br> <br>' +
          '<label>"' + challenge.name + '"</label></div>',
        showCancelButton: true,
        customClass: {
          cancelButton: 'btn btn-danger',
          confirmButton: 'btn btn-success',
        },
        confirmButtonText: 'Yes, activate it!',
        buttonsStyling: false,
        allowOutsideClick: () => !Swal.isLoading()
      }).then(async (result: any) => {
        if (result.value) {
          let challengeVm = {
            id: this.user,
            token: this.token,
            challenge: challenge.id,
          };
          await (await this._authService.activateChallenge(challengeVm)).subscribe(async data => {
            Swal.fire({
              title: 'Activated!',
              text: 'Challenge has been activated and it has no members',
              icon: 'success',
              customClass: {
                confirmButton: "btn btn-success",
              },
              buttonsStyling: false
            })
            // await this.getAllChallenges();
            challenge.active = !challenge.active;
          }, err => {
            let error = 'An error has occurred.'
            if (err.error.message) error = err.error.message;
            Swal.fire({
              title: 'Failed!',
              text: error + ' Challenge has not been activated. Please Try Again.',
              icon: 'error',
              customClass: {
                confirmButton: "btn btn-danger",
              },
              buttonsStyling: false
            })
          })
        } else {
          Swal.fire({
            title: 'Cancelled',
            text: 'Challenge has not been activated.',
            icon: 'error',
            customClass: {
              confirmButton: "btn btn-info",
            },
            buttonsStyling: false
          })
        }
      })
    }
  }



}