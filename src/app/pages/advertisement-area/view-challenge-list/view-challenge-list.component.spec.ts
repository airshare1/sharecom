import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewChallengeListComponent } from './view-challenge-list.component';

describe('ViewChallengeListComponent', () => {
  let component: ViewChallengeListComponent;
  let fixture: ComponentFixture<ViewChallengeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewChallengeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewChallengeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
