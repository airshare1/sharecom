import { first } from 'rxjs/operators';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from 'providers/authentication.service';
import { ConfigService } from 'providers/config/config.service';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { regexValidators } from 'providers/validator/validator';
import { StatsVm } from 'app/pages/dashboard/dashboard.component';
import { imagesArray } from '../view-challenge-list/view-challenge-list.component';
import * as FileSaver from "file-saver";
import { UploadFileComponent } from 'app/pages/multimedia-area/upload-file/upload-file.component';
import { ColumnMode, DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';

export class ChallengeStatsVm {
  healthPersonal: number = 0;
  healthProfessional: number = 0;
  healthGroupScore: number = 0;
  objAndKeyResultsMetric1: number = 0;
  objAndKeyResultsMetric2: number = 0;
  objAndKeyResultsMetric3: number = 0;
  objAndKeyResultsMetric4: number = 0;
}

export class CHALLENGE {
  id: string = '';
  name: string = '';
  abstract: string = '';
  overview?: any = '';
  createdAt: string = '';
  publishedAt: string = '';
  deadlineISO?: Date = null;
  deadline: string = '';
  address:string = '';
  username:string='';
  //company: string = '';
  creator: string = '';
  creatorName: string = '';
  //category: string = '';
  //category_id?: string = '';
  image: string = '';
  smallerImage1:string = '';
  smallerImage2:string = '';
  smallerImage3:string = '';
  smallerImage4:string = '';
  video?: SafeUrl = null;
  membersTotal?: number = 0;
  active: boolean = false;
  //allIdeas: [];
  //challengeStats?: ChallengeStatsVm = new ChallengeStatsVm();
}

export class Testimonial {
  name: string;
  content: string;
}

export class FILE {
  id: string = '';
  name: string = '';
  // location: string = '';
}



@Component({
  selector: 'app-view-challenge',
  templateUrl: './view-challenge.component.html',
  styleUrls: ['./view-challenge.component.css']
})
export class ViewChallengeComponent implements OnInit, OnDestroy {

  isUserAMember: boolean = false;

  user: string = '';
  token: string = '';
  id_challenge: string = '';
  isUserManager: boolean = false;
  isThereACompany: Boolean = false;
  isProfileComplete: boolean = false;
  isChallengeExpired: boolean = false;
  differenceoftime:any;
  servicecharge:any = 15;
  isThereATeam: Boolean = false;
  rows = [];

  team = {
    id: '',
    name: ''
  };
  selected;
  seeChallengeIdeas: Boolean = true;
  seeChallengeFlaggedIdeas: Boolean = false;
  seeChallengeRatedIdeas: Boolean = false;

  membersTotal: number = 0;
  public isPopupOpen = false;

  challenge: CHALLENGE = new CHALLENGE();
  stats = new StatsVm();

  public files: FILE[] = [];

  public  testimonials: Testimonial[] = [
    {
      name: "John Doe",
      content: "This challenge platform is amazing!"
    },
    {
      name: "Jane Smith",
      content: "I had a great experience participating in this challenge."
    },
    {
      name: "Jane Smith",
      content: "I had a great experience participating in this challenge."
    },
    {
      name: "Jane Smith",
      content: "I had a great experience participating in this challenge."
    },
    {
      name: "Jane Smith",
      content: "I had a great experience participating in this challenge."
    },
    // Add more testimonials here
  ];

  private subscriptionUser: Subscription;

  loading = false;

  downloadError: boolean = false;

  Toast = Swal.mixin({
    toast: true,
    position: 'bottom',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  

ColumnMode = ColumnMode;
SelectionType = SelectionType;
closeResult = '';

@ViewChild(DatatableComponent) allIdeas: DatatableComponent;


columns = [
  { prop: 'rank', name: 'Total Rank Score' },
  { prop: 'title', name: 'Title' },
  { prop: 'id', name: 'id' }
];

hoveredDate: NgbDate | null = null;

fromDate: NgbDate | null;
toDate: NgbDate | null;
content;
posts:any[]=[];
constructor(
    private _authService: AuthenticationService,
    private readonly _configService: ConfigService,
    private modalService: NgbModal,
    private router: Router,
    private sanitizer: DomSanitizer,
    private calendar: NgbCalendar, public formatter: NgbDateParserFormatter) { 
      this.fromDate = calendar.getToday();
      this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
    }



  public async ngOnInit() {

    this.subscriptionUser = this._authService.authUserChange.subscribe(async value => {
      let user: any[] = value ? JSON.parse(value) : {}
      this.user = user['_id'];
      this.token = user['token'];
      this.stats = user['stats'];

/*       if (user['company']) this.isThereACompany = true;
      else this.isThereACompany = false;

      if (user['role'] == 'Admin' || user['role'] == 'CompanyAdmin' || user['role'] == 'Manager') this.isUserManager = true;
      else this.isUserManager = false;

      // if (this.stats && this.stats.profileCompletition == 100) this.isProfileComplete = true;
      if (user['isProfileComplete']) this.isProfileComplete = true;
      else this.isProfileComplete = false; */
    });

    this.id_challenge = await this._authService.getChallenge();
    if (!this.id_challenge) this.router.navigate(['/challenge-view-list']);

    await this.getChallenge();
  }

  ngOnDestroy() {
    this.subscriptionUser.unsubscribe();
  }

  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

/*   async onActivate(event: any) {
    if (event.type === 'click') {
      console.log('idea id', event.row.id);
      const challengeVm = {
        id: this.user,
        token: this.token,
        challenge: this.id_challenge,
        idea: event.row.id
      };
  
      await (await this._authService.createChallengeTeams(challengeVm)).subscribe(data => {
  
        console.log('data', data);
        
  
      }, err => {
        console.log('error');
      });
      //TODO: Associate teams with each idea. When clicked view suggested teammembers for idea? 
      event.cellElement.blur();
    }
  } */

  getRowHeight(row) {
    if (!row) return 50;
    if (row.height === undefined) return 50;
    return row.height;
  }

  updateRowHeight(row, value: number) {
    if (value) row.height = value;
    else if (row && row.type.length > 0) row.height = (row.type.length - 1) * 20 + 50;
    else row.height = 50;
  }

  async getChallenge() {

    const challengeVm = {
      id: this.user,
      token: this.token,
      challenge: this.id_challenge
    };

    await (await this._authService.getChallengeProfile(challengeVm)).subscribe(data => {
      
      //console.log(data);
      
      this.files = [];

      const files = data.files;

      for (let index = 0; index < files.length; index++) {
        const file = files[index];
        this.files.push({
          id: file._id,
          name: file.media.name
        })
      }
      console.log('files',files);
      const fileforimage = files[0].media.location
      const fileforimage1 = files[1].media.location
      console.log(fileforimage1);
      
      if (data.team) {
        this.team.id = data.team._id;
        this.team.name = data.team.name;
        this.isThereATeam = true;
      } else {
        this.team.id = '';
        this.team.name = '';
        this.isThereATeam = false;
      }

      this.isUserAMember = data.isUserAMember;
      this.isChallengeExpired = data.isChallengeExpired;
      const challenge = data.challenge;
      //console.log(challenge);
      
      challenge.allIdeas.forEach(element => {
        console.log('element', element);
        this.rows.push({title:element.title, rank:element.stats['ranking'],id:element.stats['_id']})
      });
        
      this.rows.sort(function (a, b) {
        let X = a.rank;
        let Y = b.rank;
        if (X < Y) { return 1; }
        if (X > Y) { return -1; }
        return 0;
      });

      this.challenge = {
        id: challenge._id,
        name: challenge.name,
        createdAt: challenge.createdAt,
        publishedAt: moment(challenge.createdAt).format('Do MMM '),
        abstract: challenge.abstract,
        overview: Number(challenge.overview),
        address:challenge.address,
        // video: this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + this.youtube_parser(challenge.video)),
        video: this.youtube_parser(challenge.video),
        //company: challenge.company.name,
       // category: challenge.category.name,
        //category_id: challenge.category._id,
        deadline: moment(challenge.deadline).format('Do MMM '),
        deadlineISO: new Date(challenge.deadline),
        creator: challenge.creator._id,
        creatorName: challenge.creator.name + ' ' + challenge.creator.surname,
        membersTotal: challenge.members.length,
        active: challenge.active,
        username:challenge.username,
        //allIdeas: challenge.allIdeas,
        //challengeStats: challenge.stats,
        image: this._configService.url + fileforimage,
        smallerImage1:this._configService.url + files[1].media.location,
        smallerImage2:this._configService.url + files[2].media.location,
        smallerImage3:this._configService.url + files[3].media.location,
        smallerImage4:this._configService.url + files[4].media.location
      };
      this.rows = Object.assign([], this.rows);
      console.log(this.challenge);
      
    }, err => {
      console.log('error');
    });
  }

  async downloadFile(file: FILE) {

    this.downloadError = false;

    const multimediaVm = {
      id: this.user,
      token: this.token,
      multimedia: file.id
    };

    await (await this._authService.download(multimediaVm)).subscribe((data) => {
      this.downloadError = false;
      FileSaver.saveAs(data, 'Challenge-' + this.challenge.name + '_' + file.name);

      this.Toast.fire({
        icon: 'success',
        title: 'File has been downloaded successfully!'
      })

    }, (error) => {
      this.downloadError = true;
      console.log('error', error);
    });
  }

  youtube_parser(url) {
    // this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + this.youtube_parser(challenge.video)),
    if (url) {
      const match = url.match(regexValidators.youtube_parser);
      const newUrl = (match && match[7].length == 11) ? match[7] : false;
      return this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + newUrl);
    } else return null;
  }

  openImagePopup(): void {
    this.isPopupOpen = true;
  }

  closeImagePopup(): void {
    this.isPopupOpen = false;
  }

  




/*   async createEvent() {

    if (!this.isUserManager) return;

    const modalRef = this.modalService.open(CreateEventComponent, { size: 'lg', backdrop: 'static', centered: true });
    modalRef.componentInstance.user = this.user;
    modalRef.componentInstance.token = this.token;
    modalRef.componentInstance.challenge = this.id_challenge
    // modalRef.componentInstance.team = null;
    // modalRef.componentInstance.type = 'challenge';
    modalRef.result.then(async (result) => {
      if (result == 'success') {
        this.Toast.fire({
          icon: 'success',
          title: 'Your event has been created.'
        })
        // await this.getAllCompaniesEvents();
      }
    });
  } */

  async uploadFile() {

    if (!this.isUserManager) return;

    const modalRef = this.modalService.open(UploadFileComponent, { size: 'lg', backdrop: 'static', centered: true });
    modalRef.componentInstance.user = this.user;
    modalRef.componentInstance.token = this.token;
    modalRef.componentInstance.challenge = this.challenge.id;
    modalRef.result.then(async (result) => {
      if (result == 'success') {
        this.Toast.fire({
          icon: 'success',
          title: 'File(s) has(ve) been uploaded successfully.'
        })
      }
      await this.getChallenge();
    });
  }


  async deleteFile(file: FILE) {

    if (!this.isUserManager) return;

    Swal.fire({
      title: 'Delete File. Are you sure?',
      icon: 'warning',
      html: '<div class="form-group">' +
        '<span data-notify="message"> This file will be deleted. All the data will be deleted and the file cannot be restored.</span>' +
        '<br> <br>' +
        '<label>"' + file.name + '"</label></div>',
      showCancelButton: true,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success',
      },
      confirmButtonText: 'Yes, delete it!',
      buttonsStyling: false,
      allowOutsideClick: () => !Swal.isLoading()
    }).then(async (result: any) => {
      if (result.value) {
        const multimediaVm = {
          id: this.user,
          token: this.token,
          multimedia: file.id
        };
        await (await this._authService.deleteMultimedia(multimediaVm)).subscribe(async data => {
          Swal.fire({
            title: 'Deleted!',
            text: 'File has been deleted.',
            icon: 'success',
            customClass: {
              confirmButton: "btn btn-success",
            },
            buttonsStyling: false
          })
          setTimeout(async () => {
            await this.getChallenge();
          }, 1000);
        }, err => {
          let error = 'An error has occurred.'
          if (err.error.message) error = err.error.message;
          Swal.fire({
            title: 'Failed!',
            text: error + ' File has not been deleted. Please Try Again.',
            icon: 'error',
            customClass: {
              confirmButton: "btn btn-danger",
            },
            buttonsStyling: false
          })
        })
      } else {
        Swal.fire({
          title: 'Cancelled',
          text: 'File has not been deleted.',
          icon: 'error',
          customClass: {
            confirmButton: "btn btn-info",
          },
          buttonsStyling: false
        })
      }
    })

  }

  async goToTeam() {
    if (!this.isThereATeam) return
    await this._authService.setTeam(this.team.id);
    this.router.navigate(['/team']);
  }

  async editChallenge() {
    if (this.challenge.creator != this.user || !this.isUserManager) return;
    await this._authService.setChallenge(this.challenge.id);
    this.router.navigate(['/challenge-edit']);
  }

/*   async joinATeam() {
    if (!this.isThereACompany || !this.isChallengeExpired) return;
    const modalRef = this.modalService.open(TeamSelectionComponent, { size: 'lg', backdrop: 'static', centered: true });
    modalRef.componentInstance.user = this.user;
    modalRef.componentInstance.token = this.token;
    modalRef.componentInstance.challenge = this.id_challenge;
    modalRef.result.then(async (result) => {
      if (result == 'success') {
        await this.getChallenge();
        this.Toast.fire({
          icon: 'success',
          title: 'You have joined a team successfully.'
        })
      }
    });
  } */

  async deleteChallenge() {

    if (this.challenge.creator != this.user) return;

    Swal.fire({
      title: 'Delete Challenge. Are you sure?',
      icon: 'warning',
      html: '<div class="form-group">' +
        '<span data-notify="message"> This challenge will be deleted. All the data will be deleted and the challenge cannot be restored.</span>' +
        '<br> <br>' +
        '<label>"' + this.challenge.name + '"</label></div>',
      showCancelButton: true,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success',
      },
      confirmButtonText: 'Yes, delete it!',
      buttonsStyling: false,
      allowOutsideClick: () => !Swal.isLoading()
    }).then(async (result: any) => {
      if (result.value) {
        const challengeVm = {
          id: this.user,
          token: this.token,
          challenge: this.challenge.id
        };
        await (await this._authService.deleteChallenge(challengeVm)).subscribe(async data => {
          Swal.fire({
            title: 'Deleted!',
            text: 'Challenge has been deleted.',
            icon: 'success',
            customClass: {
              confirmButton: "btn btn-success",
            },
            buttonsStyling: false
          })
          setTimeout(() => {
            this.router.navigate(['/challenge-view-list']);
          }, 1000);
        }, err => {
          let error = 'An error has occurred.'
          if (err.error.message) error = err.error.message;
          Swal.fire({
            title: 'Failed!',
            text: error + ' Challenge has not been deleted. Please Try Again.',
            icon: 'error',
            customClass: {
              confirmButton: "btn btn-danger",
            },
            buttonsStyling: false
          })
        })
      } else {
        Swal.fire({
          title: 'Cancelled',
          text: 'Challenge has not been deleted.',
          icon: 'error',
          customClass: {
            confirmButton: "btn btn-info",
          },
          buttonsStyling: false
        })
      }
    })
  }

  async activateChallenge() {

    if (this.challenge.creator != this.user) return;

    if (this.challenge.active) {
      Swal.fire({
        title: 'Deactivate Challenge. Are you sure?',
        icon: 'warning',
        html: '<div class="form-group">' +
          '<span data-notify="message"> This challenge will be deactivated and its members will no longer have access to it.</span>' +
          '<br> <br>' +
          '<label>"' + this.challenge.name + '"</label></div>',
        showCancelButton: true,
        customClass: {
          cancelButton: 'btn btn-danger',
          confirmButton: 'btn btn-success',
        },
        confirmButtonText: 'Yes, deactivate it!',
        buttonsStyling: false,
        allowOutsideClick: () => !Swal.isLoading()
      }).then(async (result: any) => {
        if (result.value) {
          let challengeVm = {
            id: this.user,
            token: this.token,
            challenge: this.challenge.id,
          };
          await (await this._authService.activateChallenge(challengeVm)).subscribe(async data => {
            Swal.fire({
              title: 'Deactivated!',
              text: 'Challenge has been deactivated and its members will not be able to access it.',
              icon: 'success',
              customClass: {
                confirmButton: "btn btn-success",
              },
              buttonsStyling: false
            })
            // await this.getChallenge();
            this.challenge.active = !this.challenge.active;
          }, err => {
            let error = 'An error has occurred.'
            if (err.error.message) error = err.error.message;
            Swal.fire({
              title: 'Failed!',
              text: error + ' Challenge has not been deactivated. Please Try Again.',
              icon: 'error',
              customClass: {
                confirmButton: "btn btn-danger",
              },
              buttonsStyling: false
            })
          })
        } else {
          Swal.fire({
            title: 'Cancelled',
            text: 'Challenge has not been deactivated.',
            icon: 'error',
            customClass: {
              confirmButton: "btn btn-info",
            },
            buttonsStyling: false
          })
        }
      })
    } else {
      Swal.fire({
        title: 'Activate Challenge. Are you sure?',
        icon: 'warning',
        html: '<div class="form-group">' +
          '<span data-notify="message"> This challenge will be activated and it will have no members.</span>' +
          '<br> <br>' +
          '<label>"' + this.challenge.name + '"</label></div>',
        showCancelButton: true,
        customClass: {
          cancelButton: 'btn btn-danger',
          confirmButton: 'btn btn-success',
        },
        confirmButtonText: 'Yes, activate it!',
        buttonsStyling: false,
        allowOutsideClick: () => !Swal.isLoading()
      }).then(async (result: any) => {
        if (result.value) {
          let challengeVm = {
            id: this.user,
            token: this.token,
            challenge: this.challenge.id,
          };
          await (await this._authService.activateChallenge(challengeVm)).subscribe(async data => {
            Swal.fire({
              title: 'Activated!',
              text: 'Challenge has been activated and it has no members',
              icon: 'success',
              customClass: {
                confirmButton: "btn btn-success",
              },
              buttonsStyling: false
            })
            // await this.getChallenge();
            this.challenge.active = !this.challenge.active;
          }, err => {
            console.log('err', err);
            let error = 'An error has occurred.'
            if (err.error.message) error = err.error.message;
            Swal.fire({
              title: 'Failed!',
              text: error + ' Challenge has not been activated. Please Try Again.',
              icon: 'error',
              customClass: {
                confirmButton: "btn btn-danger",
              },
              buttonsStyling: false
            })
          })
        } else {
          Swal.fire({
            title: 'Cancelled',
            text: 'Challenge has not been activated.',
            icon: 'error',
            customClass: {
              confirmButton: "btn btn-info",
            },
            buttonsStyling: false
          })
        }
      })
    }
  }

  async joinChallenge() {
    if (this.isUserAMember) return

    const userVm = {
      id: this.user,
      token: this.token,
      challenge: this.id_challenge
    };

    this.loading = true;
    await (await this._authService.joinChallenge(userVm))
      .pipe(first()).subscribe(async data => {
        await this.getChallenge();
        this.loading = false;
        this.Toast.fire({
          icon: 'success',
          title: 'You have joined ' + this.challenge.name + ' challenge successfully.'
        })
      }, err => {
        this.Toast.fire({
          icon: 'error',
          title: 'An error has occurred: ' + err.error.message + ' Please try again.'
        })
        this.loading = false;
      })

  }

  async leaveChallenge() {
    if (!this.isUserAMember) return

    const userVm = {
      id: this.user,
      token: this.token,
      challenge: this.id_challenge
    };

    this.loading = true;
    await (await this._authService.leaveChallenge(userVm))
      .pipe(first()).subscribe(async data => {
        await this.getChallenge();
        this.loading = false;
        this.Toast.fire({
          icon: 'success',
          title: 'You have left ' + this.challenge.name + ' challenge successfully.'
        })
      }, err => {
        this.Toast.fire({
          icon: 'error',
          title: 'An error has occurred: ' + err.error.message + ' Please try again.'
        })
        this.loading = false;
      })
  }

  PaymentPage(){
    this.router.navigate(['/my-stripe-account']);
  }

  open(content) {
    this.modalService.open(content,
   {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = 
         `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  onDateSelection(date: NgbDate) {
		if (!this.fromDate && !this.toDate) {
			this.fromDate = date;
		} else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
			this.toDate = date;
      console.log('todate', this.toDate.day - this.fromDate.day);
      this.differenceoftime = this.toDate.day - this.fromDate.day
      
		} else {
			this.toDate = null;
			this.fromDate = date;
      console.log('fromdate', this.fromDate.day);
		}
	}

	isHovered(date: NgbDate) {
		return (
			this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate)
		);
	}

	isInside(date: NgbDate) {
		return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
	}

	isRange(date: NgbDate) {
		return (
			date.equals(this.fromDate) ||
			(this.toDate && date.equals(this.toDate)) ||
			this.isInside(date) ||
			this.isHovered(date)
		);
	}

	validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
		const parsed = this.formatter.parse(input);
		return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
	}

  // Function to open the comment/review modal
// Function to open the comment/review modal




likes(i){
  this.posts[i].likes++;
}
addComment(i,comment){
  this.posts[i].comments.push(comment)
}
addPost(){
  var obj ={
    'content':this.content,
    'likes':0,
    'comments':[]
  }
  this.posts.push(obj)
}



}



