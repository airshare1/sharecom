import { Component, OnInit, ElementRef, Injectable, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'providers/authentication.service';
import { regexValidators } from 'providers/validator/validator';
import { NgbDate, NgbCalendar, NgbDateAdapter, NgbDateParserFormatter, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';


declare var $: any;

export const MAXFILES = 100;

/**
 * This Service handles how the date is represented in scripts i.e. ngModel.
 */
@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {

  readonly DELIMITER = '-';

  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      const date = value.split(this.DELIMITER);
      return {
        day: parseInt(date[0], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[2], 10)
      };
    }
    return null;
  }

  toModel(date: NgbDateStruct | null): string | null {
    return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null;
  }
}

/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */
@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '/';

  parse(value: string): NgbDateStruct | null {
    if (value) {
      const date = value.split(this.DELIMITER);
      return {
        day: parseInt(date[0], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[2], 10)
      };
    }
    return null;
  }

  format(date: NgbDateStruct | null): string {
    return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : '';
  }
}

@Component({
  selector: 'app-create-challenge',
  templateUrl: './create-challenge.component.html',
  styleUrls: ['./create-challenge.component.css'],
  providers: [
    { provide: NgbDateAdapter, useClass: CustomAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter }
  ]
})
export class CreateChallengeComponent implements OnInit, OnDestroy {

  focus;
  focus1;
  focus2;
  focus3;
  focus4;
  focus5;
  focus6;
  focus7;
  focus8;

  manager: boolean = false;
  selectedPriceOption: string = 'specific'; // Default to 'Specific Price'
  selectedPrice: string = ''; // Default empty value
  user: string = '';
  token: string = '';

  allCategoriesRecords: any;

  challengeform: FormGroup;
  loading = false;
  submitted = false;
  success = false;
  errorQuantity = '';
  errorSize = '';
  error = '';
  returnUrl = '';
  hide = true;
  showError = false;

  // fileToUpload: File;
  fileToUpload: Array<File> = [];


  hoveredDate: NgbDate | null = null;

	fromDate: NgbDate | null;
	toDate: NgbDate | null;

  private subscriptionUser: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private modalService: NgbModal,
    private _authService: AuthenticationService,
    private ngbCalendar: NgbCalendar,
    public formatter: NgbDateParserFormatter,
    private dateAdapter: NgbDateAdapter<string>) {

      this.fromDate = ngbCalendar.getToday();
      this.toDate = ngbCalendar.getNext(ngbCalendar.getToday(), 'd', 10);

     }


  async ngOnInit() {
    // Register the validators
    this.challengeform = this.formBuilder.group({
      name: ['', Validators.required],
      abstract: ['', Validators.required],
      overview: ['', Validators.required],
      address: ['', Validators.required],
      deadline: ['', Validators.required],
      video: [null, Validators.pattern(regexValidators.youtube)],
      checked: [false, Validators.requiredTrue],
    });

    this.subscriptionUser = this._authService.authUserChange.subscribe(async value => {
      let user: any[] = value ? JSON.parse(value) : {}
      this.user = user['_id'];
      this.token = user['token'];

/*        if (user['role'] == 'Admin' || user['role'] == 'CompanyAdmin' || user['role'] == 'Manager') this.manager = true;
      else this.manager = false;  */

/*       if (!this.manager) this.router.navigate(['/challenge-view-list']); */
    });

/*     await this.getAllCategories(); */
  }

  get today() {
    return this.dateAdapter.toModel(this.ngbCalendar.getToday())!;
  }

/*   async getAllCategories() {

    const categoryVm = {
      id: this.user,
      token: this.token,
      type: 'challenge'
    };

    await (await this._authService.getAllCategoriesByType(categoryVm)).subscribe(newData => {
      this.allCategoriesRecords = [];
      this.categoriesRecords = [];
      this.allCategoriesRecords = newData;
      for (let index = 0; index < this.allCategoriesRecords.length; index++) {
        const category = this.allCategoriesRecords[index];
        this.categoriesRecords.push({
          id: category._id,
          name: category.name,
          type: category.type
        })
      }
      this.categoriesRecords.sort(function (a, b) {
        if (a.name < b.name) { return -1; }
        if (a.name > b.name) { return 1; }
        return 0;
      });

    }, err => {
      console.log('error');
    });
  } */

  ngOnDestroy() {
    this.subscriptionUser.unsubscribe();
  }

  // Easy access to the inputs
  get f() { return this.challengeform.controls; }

  async seeTerms() {
    console.log('Terms');
    // const modalRef = this.modalService.open(TermsComponent, { size: 'lg', centered: true });
  }
  async seePrivacy() {
    console.log('Privacy');
    // const modalRef = this.modalService.open(PrivacyComponent, { size: 'lg', centered: true });
  }

  fileChangeEvent(fileInput: any) {

    if (!fileInput) this.fileToUpload = [];
    else if (fileInput.target.files && fileInput.target.files.length < 100)
      for (let index = 0; index < fileInput.target.files.length; index++)
        this.fileToUpload.push(<File>fileInput.target.files[index]);
    this.checkFile();
  }

  removeItem(index: number) {
    this.fileToUpload.splice(index, 1);
    this.checkFile();
  }

  checkFile() {
    if (this.fileToUpload.length > MAXFILES) this.errorQuantity = 'Too many files';
    else this.errorQuantity = '';

    const isTooBig = this.fileToUpload.some(file => file.size > 500 * 1e+6);

    if (!isTooBig) this.errorSize = ''
    else this.errorSize = 'File exceeds the maximum upload size.'

    if (this.fileToUpload.length == 0) $(".fileinput").fileinput("clear");
  }

  async onSubmit() {

    this.submitted = true;
    this.showError = false;
    this.success = false;

    // If there was an error in validation
    if (this.challengeform.invalid) return;

    // Disable the button and show the spinner
    this.loading = true;

    const challengeVm = {
      id: this.user,
      token: this.token,
      name: this.f.name.value,
      abstract: this.f.abstract.value,
      overview: this.f.overview.value,
      address: this.f.address.value,
      deadline: this.f.deadline.value,
      video: this.f.video.value,
    };

    if (this.fileToUpload) {
      let formData: any = new FormData();
      for (let index = 0; index < this.fileToUpload.length; index++)
        formData.append('file', this.fileToUpload[index]);
        console.log(formData);
        
      Object.keys(challengeVm).forEach(key => formData.append(key, challengeVm[key]));

      await (await this._authService.createChallengeUploadMultimedia(formData))
        .pipe(first()).subscribe(data => {
          console.log(data);
          this.loading = false;
          this.submitted = false;
          this.success = true;
        }, err => {
          console.log(err);
          this.showError = true;
          // this.error = err || 'There was an error';
          this.loading = false;
          this.submitted = false;
        })
    } else {
      await (await this._authService.createChallenge(challengeVm))
        .pipe(first()).subscribe(data => {
          this.loading = false;
          this.submitted = false;
          this.success = true;
        }, err => {
          this.showError = true;
          // this.error = err || 'There was an error';
          this.loading = false;
          this.submitted = false;
        })
    }
  }

  onDateSelection(date: NgbDate) {
		if (!this.fromDate && !this.toDate) {
			this.fromDate = date;
		} else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
			this.toDate = date;
		} else {
			this.toDate = null;
			this.fromDate = date;
		}
	}

	isHovered(date: NgbDate) {
		return (
			this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate)
		);
	}

	isInside(date: NgbDate) {
		return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
	}

	isRange(date: NgbDate) {
		return (
			date.equals(this.fromDate) ||
			(this.toDate && date.equals(this.toDate)) ||
			this.isInside(date) ||
			this.isHovered(date)
		);
	}

	validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
		const parsed = this.formatter.parse(input);
		return parsed && this.ngbCalendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
	}

  priceOptionChanged(option: string) {
    this.selectedPriceOption = option;
    if (option === 'free') {
      this.selectedPrice = 'Free';
      this.challengeform.get('overview').setValue('Free');
    } else {
      this.selectedPrice = '';
      this.challengeform.get('overview').setValue('');
    }
  }
}

