import { Component, OnDestroy, OnInit, } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthenticationService } from 'providers/authentication.service';
import { first } from 'rxjs/operators';

declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  // styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {

  constructor(private formBuilder: FormBuilder,
    private _authService: AuthenticationService) {
  }

  loginform: FormGroup;

  loading = false;
  submitted = false;
  error = '';
  showError = false;

  succeed = false;

  focus;

  imageURL = '';

  async ngOnInit() {

    this.checkFullPageBackgroundImage();

    this.loginform = this.formBuilder.group({
      email: ['', [Validators.required, Validators.minLength(6), Validators.email]],
    });

    await this._authService.logout();
  }

  checkFullPageBackgroundImage() {
    var $page = $('.full-page');
    var image_src = $page.data('image');
    var body = document.getElementsByTagName('body')[0];
    body.classList.add('login-page');
    if (image_src !== undefined) {
      var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>'
      $page.append(image_container);
    }
  };

  ngOnDestroy() {
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('login-page');
  }

  get f() { return this.loginform.controls; }

  async onSubmit() {

    this.submitted = true;
    this.showError = false;

    if (this.loginform.invalid) return;

    let forgotPasswordVm = {
      email: this.f.email.value,
    }

    this.loading = true;
    await (await this._authService.forgotPassword(forgotPasswordVm))
      .pipe(first()).subscribe(data => {
        this.loading = false;
        this.submitted = false;
        this.succeed = true;
      }, err => {
        this.showError = true;
        this.error = err || 'There was an error'
        this.loading = false;
        this.submitted = false;
      })
  }

}
