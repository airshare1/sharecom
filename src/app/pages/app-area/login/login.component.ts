import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StoreProvider } from 'providers/store/store';
import { AuthenticationService } from 'providers/authentication.service';
import { first } from 'rxjs/operators';

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'login-cmp',
    styleUrls: ["./login.component.css"],
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit, OnDestroy {

    hide = true;

    loginform: FormGroup;
    loading = false;
    submitted = false;
    error = '';
    showError = false;
    showErrorConnection = false;

    focus;
    focus1;
    focus2;

    constructor(
        private formBuilder: FormBuilder,
        private storage: StoreProvider,
        private _authService: AuthenticationService) {
    }

    checkFullPageBackgroundImage() {
        var $page = $('.full-page');
        var image_src = $page.data('image');
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');
        if (image_src !== undefined) {
            var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>'
            $page.append(image_container);
        }
    };

    async ngOnInit() {

        this.checkFullPageBackgroundImage();

        this.loginform = this.formBuilder.group({
            email: ['', [Validators.required, Validators.minLength(6), Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });

        await this._authService.logout();
    }

    ngOnDestroy() {
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('login-page');
    }


    get f() { return this.loginform.controls; }

    async onSubmit() {
        this.submitted = true;
        this.showError = false;

        // If there was an error in validation
        if (this.loginform.invalid) return;

        const loginVm = {
            email: this.f.email.value,
            password: this.f.password.value,
            // firebaseToken: await this.storage.getItem('firebaseToken')
        };

        // Disable the button and show the spinner
        this.loading = true;

        await (await this._authService.login(loginVm)).pipe(first()).subscribe(data => {
            this.loading = false;
            this.submitted = false;
            this.ngOnDestroy();
        }, err => {
            if (err.status == 0) this.showErrorConnection = true;
            else {
                this.showError = true;
                this.error = err || 'There was an error'
            }
            this.loading = false;
            this.submitted = false;
        })
    }

}