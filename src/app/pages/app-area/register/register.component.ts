import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'providers/authentication.service';
import { regexValidators } from 'providers/validator/validator';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { first } from 'rxjs/operators';

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'register-cmp',
    templateUrl: './register.component.html'
})

export class RegisterComponent implements OnInit {

    focus;
    focus1;
    focus2;
    focus3;
    focus4;
    focus5;
    focus6;
    focus7;
    focus8;

    registerform: FormGroup;
    loading = false;
    submitted = false;
    success = false;
    error = '';
    returnUrl = '';
    hide = true;
    showError = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private modalService: NgbModal,
        private _authService: AuthenticationService,) { }

    checkFullPageBackgroundImage() {
        var $page = $('.full-page');
        var image_src = $page.data('image');
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('register-page');
        if (image_src !== undefined) {
            var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>'
            $page.append(image_container);
        }
    };

    async ngOnInit() {
        this.checkFullPageBackgroundImage();
        // Register the validators
        this.registerform = this.formBuilder.group({
            name: ['', Validators.required],
            surname: ['', Validators.required],
            email: ['', [Validators.compose([Validators.pattern(regexValidators.email), Validators.required, Validators.minLength(6), Validators.email])]],
            password: ['', [Validators.compose([Validators.pattern(regexValidators.password), Validators.required, Validators.minLength(6)])]],
            checked: [false, Validators.requiredTrue],
            confirmpassword: ['', [Validators.compose([Validators.pattern(regexValidators.password), Validators.required, Validators.minLength(6)])]],
        }, { validator: this.matchingPasswords('password', 'confirmpassword') }
        );

        // Reset the login state
        await this._authService.logout();
    }

    ngOnDestroy() {
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('register-page');
    }
    // Easy access to the inputs
    get f() { return this.registerform.controls; }

    matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
        return (group: FormGroup): { [key: string]: any } => {
            let password = group.controls[passwordKey];
            let confirmPassword = group.controls[confirmPasswordKey];
            if (password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        }
    }

    async seeTerms() {
        console.log('Terms');
        // const modalRef = this.modalService.open(TermsComponent, { size: 'lg', centered: true });
    }
    async seePrivacy() {
        console.log('Privacy');
        // const modalRef = this.modalService.open(PrivacyComponent, { size: 'lg', centered: true });
    }


    async onSubmit() {

        this.submitted = true;
        this.showError = false;
        this.success = false;
        console.log('', );
        // If there was an error in validation
        if (this.registerform.invalid) return;

        // Disable the button and show the spinner
        this.loading = true;

        let register = {
            name: this.f.name.value,
            surname: this.f.surname.value,
            email: this.f.email.value,
            password: this.f.password.value,
            // company: this.f.company.value,
            // industry: this.f.industry.value,
            // companyRole: this.f.companyRole.value,
        };
        console.log('register', register);
        await (await this._authService.register(register))
            .pipe(first()).subscribe(data => {
                this.loading = false;
                this.submitted = false;
                this.success = true;
                // setTimeout(() => {
                //     this.router.navigate(['/login']);
                // }, 10000);
            }, err => {
                this.showError = true;
                this.error = err || 'There was an error';
                this.loading = false;
                this.submitted = false;
            })

    }
}
