import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { UploadPhotosComponent } from 'app/pages/multimedia-area/upload-photos/upload-photos.component';
import { UploadProfilePictureComponent } from 'app/pages/multimedia-area/upload-profile-picture/upload-profile-picture.component';
import { AuthenticationService } from 'providers/authentication.service';
import { ConfigService } from 'providers/config/config.service';
import { StoreProvider } from 'providers/store/store';
import { regexValidators } from 'providers/validator/validator';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import Swal from 'sweetalert2';
import Swiper, { Navigation, Pagination } from "swiper";
import { PhotoViewer } from "@awesome-cordova-plugins/photo-viewer/ngx";
import { IDropdownSettings } from "ng-multiselect-dropdown";
import Viewer from 'viewerjs';


declare var $: any;

export class TEAM {
    id: string = '';
    name: string = '';
    description: string = '';
    active: boolean = false;
}

@Component({
    moduleId: module.id,
    selector: 'user-cmp',
    styleUrls: ["./user.component.css"],
    templateUrl: 'user.component.html'
})

export class UserComponent {

    private subscriptionUser: Subscription;
    profileform: FormGroup;
    profilebioform: FormGroup;
    passwordform: FormGroup;

    firebaseToken = '';

    allowEdit = false;
    allowUpdateBio = false;
    allowChangePassword = false;

    submitted = false;
    showError = false;
    success = false;
    loading = false;
    error = '';

    submittedPassword = false;
    showErrorPassword = false;
    successPassword = false;
    loadingPassword = false;
    errorPassword = '';

    submittedBio = false;
    showErrorBio = false;
    successBio = false;
    loadingBio = false;
    errorBio = "";

    hide = true;

    user = {
        id: '',
        token: '',
        name: '',
        surname: '',
        fullName: '',
        email: '',
        avatar: '',
        bio: [],
        photos: [],
       // company: '',
       // companyRole: '',
        emailVerification: null
    };
    	
    isTherePicture: boolean = false;
    isDeviceMobile: boolean = false;
    Toast = Swal.mixin({
        toast: true,
        position: "bottom",
        showConfirmButton: false,
        timer: 5000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener("mouseenter", Swal.stopTimer);
          toast.addEventListener("mouseleave", Swal.resumeTimer);
        },
      });
    
    constructor(
        private formBuilder: FormBuilder,
        private _authService: AuthenticationService,
        private storage: StoreProvider,
        private router: Router,
        private photoViewer: PhotoViewer,
        private modalService: NgbModal,
        private readonly _configService: ConfigService
    ) { }

    ngOnInit() {

        this.subscriptionUser = this._authService.authUserChange.subscribe(async value => {
            console.log(value)
            let user: any[] = value ? JSON.parse(value) : {}
            
            this.user.id = user['_id'];
            this.user.token = user['token'];
            this.user.fullName = user['name'] + ' ' + user['surname'];
            this.user.name = user['name'];
            this.user.surname = user['surname'];
            this.user.email = user['email'];
            this.user.bio = user["bio"] ? user["bio"].split("<br>") : [];
            // this.user.company = user['company'].name;
            //this.user.companyRole = user['companyRole'];
            this.user.avatar = this._configService.url + user['avatar'];
           this.isTherePicture =
            user["avatar"] != "resources/assets/avatar/avatar.png" ? true : false;
            console.log(this.isTherePicture)
            console.log(this.user)
/*           this.user.photos = [];
  
          for (let i = 0; i < user["photos"].length; i++)
            this.user.photos.push(
              this._configService.url + user["photos"][i].media.location
            ); */
            //if (user['company']) this.user.company = user['company'].name;
        });

        this.profileform = this.formBuilder.group({
            name: [this.user.name, Validators.required],
            surname: [this.user.surname, Validators.required],
            //company: [this.user.company, Validators.required],
            //companyRole: [this.user.companyRole, Validators.required],
            email: [this.user.email, [Validators.compose([Validators.pattern(regexValidators.email), Validators.required, Validators.minLength(6), Validators.email])]]
        });

        this.profilebioform = this.formBuilder.group({
            bio: [this.user.bio.join("\n")],
          });

        this.passwordform = this.formBuilder.group({
            password: ['', Validators.required],
            newpassword: ['', [Validators.compose([Validators.pattern(regexValidators.password), Validators.required, Validators.minLength(6)])]],
            confirmnewpassword: ['', [Validators.compose([Validators.pattern(regexValidators.password), Validators.required, Validators.minLength(6)])]],
        }, { validator: this.matchingPasswords('newpassword', 'confirmnewpassword') }
        );

        const swiperImage = new Swiper(".swiper-photos", {
            // configure Swiper to use modules
            slidesPerView: 3,
            spaceBetween: 30,
            navigation: {
              nextEl: ".swiper-button-next",
              prevEl: ".swiper-button-prev",
            },
            modules: [Navigation, Pagination],
          });
      
          const swiperTestimonials = new Swiper(".swiper-testimonials", {
            // configure Swiper to use modules
            // slidesPerView: 3,
            spaceBetween: 30,
            navigation: {
              nextEl: ".swiper-button-next",
              prevEl: ".swiper-button-prev",
            },
            modules: [Navigation, Pagination],
          });
    }

    get f() { return this.profileform.controls; }
    get fpass() { return this.passwordform.controls; }
    get fbio() {
        return this.profilebioform.controls;
      }

    matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
        return (group: FormGroup): { [key: string]: any } => {
            let password = group.controls[passwordKey];
            let confirmPassword = group.controls[confirmPasswordKey];
            if (password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        }
    }

    ngOnDestroy() {
        this.subscriptionUser.unsubscribe();
    }
    
    openImage(photoURL: string) {
        if (!this.isDeviceMobile) {
          const gallery = new Viewer(document.getElementById("images"));
          gallery.show();
        } else {
          this.photoViewer.show(photoURL);
        }
      }
    
      async uploadProfilePicture() {
        const modalRef = this.modalService.open(UploadProfilePictureComponent, {
          size: "lg",
          backdrop: "static",
          centered: true,
        });
        modalRef.componentInstance.user = this.user.id;
        modalRef.componentInstance.token = this.user.token;
        modalRef.componentInstance.avatar = this.user.avatar;
        modalRef.result.then(async (result) => {
          if (result == "success") {
            this.Toast.fire({
              icon: "success",
              title: "Your profile picture has been uploaded successfully.",
            });
          }
        });
      }
    
      async uploadPhotos() {
        const modalRef = this.modalService.open(UploadPhotosComponent, {
          size: "lg",
          backdrop: "static",
          centered: true,
        });
        modalRef.componentInstance.user = this.user.id;
        modalRef.componentInstance.token = this.user.token;
        modalRef.result.then(async (result) => {
          if (result == "success") {
            this.Toast.fire({
              icon: "success",
              title: "Your photo(s) has(ve) been uploaded successfully.",
            });
          }
        });
      }

    async onSubmit() {

        this.submitted = true;
        this.showError = false;
        this.success = false;

        if (this.profileform.invalid) return;

        this.loading = true;

        const userVm = {
            id: this.user.id,
            token: this.user.token,
            name: this.f.name.value,
            surname: this.f.surname.value,
            email: this.f.email.value,
            //company: this.f.company.value,
            //companyRole: this.f.companyRole.value,
        };

        await (await this._authService.EditUser(userVm))
            .pipe(first()).subscribe(data => {
                this.loading = false;
                this.submitted = false;
                this.success = true;
                this.allowEdit = false;
                setTimeout(() => {
                    this.success = false;
                }, 15000);
            }, err => {
                console.log('err', err);
                this.showError = true;
                this.error = err.error.response || 'There was an error'
                this.loading = false;
                this.submitted = false;
            })
    }

    async onSubmitBio() {
        this.submittedBio = true;
        this.showErrorBio = false;
        this.successBio = false;
    
        if (this.profilebioform.invalid) return;
    
        this.loadingBio = true;
    
        const userVm = {
          id: this.user.id,
          token: this.user.token,
          bio: this.fbio.bio.value.replace(/(?:\r\n|\r|\n)/g, "<br>"),
        };
    
        await (await this._authService.EditUserBio(userVm)).pipe(first()).subscribe(
          (data) => {
            this.loadingBio = false;
            this.submittedBio = false;
            this.successBio = true;
            this.allowUpdateBio = false;
            setTimeout(() => {
              this.successBio = false;
            }, 15000);
          },
          (err) => {
            console.log("err", err);
            this.showErrorBio = true;
            this.errorBio = err.error.response || "There was an error";
            this.loadingBio = false;
            this.submittedBio = false;
          }
        );
      }

    async onSubmitPassword() {

        this.submittedPassword = true;
        this.showErrorPassword = false;
        this.successPassword = false;

        if (this.passwordform.invalid) return;

        this.loadingPassword = true;

        const passwordVm = {
            id: this.user.id,
            token: this.user.token,
            currentpassword: this.fpass.password.value,
            password: this.fpass.newpassword.value,
        };
        await (await this._authService.EditPassword(passwordVm))
            .pipe(first()).subscribe(data => {
                this.loadingPassword = false;
                this.successPassword = true;
                this.allowChangePassword = false;
                this.passwordform.reset();
                this.submittedPassword = false;
                setTimeout(() => {
                    this.successPassword = false;
                }, 15000);
            }, err => {
                this.loadingPassword = false;
                this.showErrorPassword = true;
                setTimeout(() => {
                    this.submittedPassword = false;
                }, 15000);
            });
    }

    async deleteUser() {
        Swal.fire({
            title: 'Delete account. Are you sure?',
            icon: 'warning',
            html: '<div class="form-group">' +
                '<span data-notify="message"> This account will be deleted and you will no longer have access to Template app. All the data will be deleted and the account cannot be restored.</span>' +
                '<br> <br>' +
                '<label>If so, enter your password</label>' +
                '<input id="input-field" type="password" class="form-control"/>' +
                '</div>',
            showCancelButton: true,
            customClass: {
                cancelButton: 'btn btn-danger',
                confirmButton: 'btn btn-success',
            },
            confirmButtonText: 'Yes, delete it!',
            buttonsStyling: false,
            preConfirm: () => {
                const password = $('#input-field').val();
                if (!password)
                    Swal.showValidationMessage(
                        'Password is required'
                    )
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then(async (result: any) => {
            const password = $('#input-field').val();
            if (result.value && password) {
                let userVm = {
                    id: this.user.id,
                    token: this.user.token,
                    password: password
                };
                await (await this._authService.deleteUserAccount(userVm)).subscribe(async data => {
                    Swal.fire({
                        title: 'Deleted!',
                        text: 'Your account has been deleted and you have been logged out from Template application.',
                        icon: 'success',
                        customClass: {
                            confirmButton: "btn btn-success",
                        },
                        buttonsStyling: false
                    })
                    // Remove all tokens and the user data
                    await this._authService.logout();
                    this.firebaseToken = await this.storage.getItem('firebaseToken');
                    await this.storage.clear();
                    await this.storage.setItem('firebaseToken', this.firebaseToken);
                    this._authService.setIsAuthorized(false);
                    this.router.navigate(['/login']);
                }, err => {
                    console.log('err', err);
                    let error = 'An error has occurred.'
                    if (err.error.message) error = err.error.message;
                    Swal.fire({
                        title: 'Failed!',
                        text: error + ' Your account has not been deleted. Please Try Again.',
                        icon: 'error',
                        customClass: {
                            confirmButton: "btn btn-danger",
                        },
                        buttonsStyling: false
                    })
                })
            } else {
                Swal.fire({
                    title: 'Cancelled',
                    text: 'Your account had not been deleted.',
                    icon: 'error',
                    customClass: {
                        confirmButton: "btn btn-info",
                    },
                    buttonsStyling: false
                })
            }
        })
    }

    async removeProfilePicture() {
      if (!this.isTherePicture) return;
  
      Swal.fire({
        title: "Delete Profile Picture. Are you sure?",
        icon: "warning",
        html:
          '<div class="form-group">' +
          '<span data-notify="message"> Your profile picture will be removed and the avatar picture will be restored. The data will be deleted and it cannot be restored.</span>' +
          // '<br> ' +
          "</div>",
        showCancelButton: true,
        customClass: {
          cancelButton: "btn btn-danger",
          confirmButton: "btn btn-success",
        },
        confirmButtonText: "Yes, delete it!",
        buttonsStyling: false,
        allowOutsideClick: () => !Swal.isLoading(),
      }).then(async (result: any) => {
        if (result.value) {
          let userVm = {
            id: this.user.id,
            token: this.user.token,
          };
          await (
            await this._authService.resetProfilePicture(userVm)
          ).subscribe(
            async (data) => {
              Swal.fire({
                title: "Deleted!",
                text: "Your profile picture has been removed and the avatar picture restored.",
                icon: "success",
                customClass: {
                  confirmButton: "btn btn-success",
                },
                buttonsStyling: false,
              });
            },
            (err) => {
              console.log("err", err);
              let error = "An error has occurred.";
              if (err.error.message) error = err.error.message;
              Swal.fire({
                title: "Failed!",
                text:
                  error +
                  " Your profile picture has not been deleted. Please Try Again.",
                icon: "error",
                customClass: {
                  confirmButton: "btn btn-danger",
                },
                buttonsStyling: false,
              });
            }
          );
        } else {
          Swal.fire({
            title: "Cancelled",
            text: "Your profile picture had not been deleted.",
            icon: "error",
            customClass: {
              confirmButton: "btn btn-info",
            },
            buttonsStyling: false,
          });
        }
      });
    }
    // const modalRef = this.modalService.open(DeleteAccount, { size: 'lg', backdrop: 'static', centered: true });
    // modalRef.componentInstance.deletedUser = this.userDetails;
    // modalRef.componentInstance.user = this.userDetails.id;
    // modalRef.componentInstance.token = this.userDetails.token;
    // modalRef.result.then(async (result) => {
    //     if (result == 'success') {
    //         // Remove all tokens and the user data
    //         await this._authService.logout();
    //         // Set authorized to not true
    //         this.firebaseToken = await this.storage.getItem('firebaseToken');
    //         await this.storage.clear();
    //         await this.storage.setItem('firebaseToken', this.firebaseToken);
    //         this._authService.setIsAuthorized(false);
    //         this.router.navigate(['/login']);
    //     }
    // });
}

// @Component({
//     selector: 'app-profile-settings-delete',
//     templateUrl: './profile-settings-delete.component.html',
//     styleUrls: ['./profile-settings.component.scss']
// })
// export class DeleteAccount implements OnInit {

//     @Input() deletedUser;
//     @Input() user;
//     @Input() token;

//     constructor(public activeModal: NgbActiveModal,
//         private _authService: AuthenticationService) {
//     }

//     async ngOnInit() {
//     }

//     async deleteProject() {
//         let userVm = {
//             id: this.user,
//             token: this.token,
//             user: this.deletedUser.id
//         };

//         await (await this._authService.deleteUser(userVm)).subscribe(data => {
//             this.activeModal.close('success');
//         }, err => {
//             this.activeModal.close('error');
//         })
//     }
// }

