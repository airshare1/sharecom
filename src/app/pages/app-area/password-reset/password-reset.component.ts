import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'providers/authentication.service';
import { regexValidators } from 'providers/validator/validator';
import { first } from 'rxjs/operators';

declare var $: any;

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css']
})
export class PasswordResetComponent implements OnInit, OnDestroy {

  passwordID: string = null;
  passwordForm: FormGroup;

  loading = false;
  loadingPage = true;

  submitted = false;
  error = '';
  showError = false;
  succeed = false;

  hide = true;
  requestExpired = false;

  imageURL = '';

  focus;
  focus1;

  constructor(private formBuilder: FormBuilder,
    private _authService: AuthenticationService,
    private route: ActivatedRoute) { }

  async ngOnInit() {

    this.checkFullPageBackgroundImage();

    this.passwordForm = this.formBuilder.group({
      password: ['', [Validators.compose([Validators.pattern(regexValidators.password), Validators.required, Validators.minLength(6)])]],
      confirmpassword: ['', [Validators.compose([Validators.pattern(regexValidators.password), Validators.required, Validators.minLength(6)])]],
    }, { validator: this.matchingPasswords('password', 'confirmpassword') }
    );

    // http://localhost:4200/password-reset/6022991efab7910a6471018a

    this.loadingPage = true;

    await this._authService.logout();

    this.passwordID = await this.route.snapshot.paramMap.get('id');
    await this.checkRequestExpiration();
  }

  checkFullPageBackgroundImage() {
    var $page = $('.full-page');
    var image_src = $page.data('image');
    var body = document.getElementsByTagName('body')[0];
    body.classList.add('login-page');
    if (image_src !== undefined) {
      var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>'
      $page.append(image_container);
    }
  };

  ngOnDestroy() {
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('login-page');
  }

  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

  get f() { return this.passwordForm.controls; }

  async onSubmit() {

    this.submitted = true;
    this.showError = false;

    if (this.passwordForm.invalid) return;

    let forgotPasswordVm = {
      id: this.passwordID,
      password: this.f.password.value,
    };

    this.loading = true;
    await (await this._authService.resetPassword(forgotPasswordVm))
      .pipe(first()).subscribe(data => {
        this.loading = false;
        this.submitted = false;
        this.succeed = true;
      }, err => {
        this.showError = true;
        this.error = err || 'There was an error'
        this.loading = false;
        this.submitted = false;
      })
  }

  async checkRequestExpiration() {

    this.requestExpired = false;

    let resetPasswordVm = {
      id: this.passwordID
    };

    await (await this._authService.checkPasswordExpiration(resetPasswordVm))
      .pipe(first()).subscribe(data => {
        this.loadingPage = false;
        this.requestExpired = false;
      }, err => {
        this.loadingPage = false;
        this.requestExpired = true;
      })
  }
}
