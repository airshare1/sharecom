import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { userRoleType } from 'app/configurations/dropdownConfigurations';
import { AuthenticationService } from 'providers/authentication.service';
import { ConfigService } from 'providers/config/config.service';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import Swal from 'sweetalert2';

declare var $: any;

export interface USER {
  id: string;
  fullName: string;
  name: string;
  surname: string;
  role: string;
  email: string;
  publicid: string;
  avatar: string;
  enable: boolean;
}

@Component({
  selector: 'app-admin-user',
  templateUrl: './admin-user.component.html',
  styleUrls: ['./admin-user.component.css']
})
export class AdminUserComponent implements OnInit, OnDestroy {
[x: string]: any;

  admin: boolean = false;

  user: string = '';
  token: string = '';

  allUsersRecords: any;
  public usersRecords: USER[] = [];
  public usersRecordsTemp: USER[] = [];


  private subscriptionUser: Subscription;

  userRoleType = userRoleType;

  Toast = Swal.mixin({
    toast: true,
    position: 'bottom',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  constructor(
    private _authService: AuthenticationService,
    private readonly _configService: ConfigService,
    private modalService: NgbModal,
    private router: Router
  ) { }

  async ngOnInit() {

    this.subscriptionUser = this._authService.authUserChange.subscribe(async value => {
      let user: any[] = value ? JSON.parse(value) : {}
      this.user = user['_id'];
      this.token = user['token'];

      if (user['role'] == 'Admin') this.admin = true;
      else this.admin = false;

      if (!this.admin) this.router.navigate(['/dashboard']);
    });

    await this.getAllUsers();

  }

  SearchItems(event) {
    const val = event.target.value.toLowerCase();
    // this.teamsRecordsTemp.forEach(item => Object.keys(item).forEach(k => item[k] = (item[k] == null || item[k] == undefined) ? '' : item[k]));
    const temp = this.usersRecordsTemp.filter(function (d) {
      return d.name.toString().toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.usersRecords = temp;
  }

  async getAllUsers() {

    const userVm = {
      id: this.user,
      token: this.token,
    };

    await (await this._authService.getAdminAllUsers(userVm)).subscribe(newData => {
      this.allUsersRecords = [];
      this.usersRecords = [];
      this.allUsersRecords = newData;
      for (let index = 0; index < this.allUsersRecords.length; index++) {
        const user = this.allUsersRecords[index];

        this.usersRecords.push({
          id: user._id,
          fullName: user['name'] + ' ' + user['surname'],
          name: user['name'],
          surname: user['surname'],
          email: user['email'],
          enable: user['enable'],
          role: user['role'],
          publicid: user['publicid'],
          avatar: this._configService.url + user['avatar']
        })
      }
      this.usersRecords.sort(function (a, b) {
        if (a.name < b.name) { return -1; }
        if (a.name > b.name) { return 1; }
        return 0;
      });

      this.usersRecordsTemp = this.usersRecords;

    }, err => {
      console.log('error');
    });
  }

  async moreInfo(user: USER) {
    // TBD
  }

  async updateUserRole(user: USER, role: string) {

    if (!role) {
      this.Toast.fire({
        icon: 'warning',
        title: 'No role has been selected.'
      })
      return
    }

    if (user.id == this.user) {
      this.Toast.fire({
        icon: 'warning',
        title: 'You cannot change your own role.'
      })
      await this.getAllUsers();
      return
    }

    const userVm = {
      id: this.user,
      token: this.token,
      user: user.id,
      role: role
    };

    await (await this._authService.changeAdminUserRole(userVm))
      .pipe(first()).subscribe(async data => {
        this.Toast.fire({
          icon: 'success',
          title: 'The user\'s role has been updated successfully.'
        })
        await this.getAllUsers();
      }, async err => {
        this.Toast.fire({
          icon: 'error',
          title: 'An error has occurred. Please try again.'
        })
        await this.getAllUsers();
      })
  }


  async enableUser(user: USER) {

    if (user.id == this.user) return;

    if (user.enable) {
      Swal.fire({
        title: '<p class="text-white">Disable User Account. Are you sure?</p>',
        icon: 'warning',
        html: '<div class="form-group text-white">' +
          '<span data-notify="message"> This user\'s account will be disabled and the user will no longer have access to Cardio Flourish app.</span>' +
          '<br> <br>' +
          '<label>"' + user.fullName + '"</label>' +
          '<br> <br>' +
          '<label>If so, enter your password</label>' +
          '<input id="input-field" type="password" class="form-control"/>' +
          '</div>',
        showCancelButton: true,
        background: '#000',
        customClass: {
          cancelButton: 'btn btn-danger',
          confirmButton: 'btn btn-success',
        },
        confirmButtonText: 'Yes, disable it!',
        buttonsStyling: false,
        preConfirm: () => {
          const password = $('#input-field').val();
          if (!password)
            Swal.showValidationMessage(
              'Password is required'
            )
        },
        allowOutsideClick: () => !Swal.isLoading()
      }).then(async (result: any) => {
        const password = $('#input-field').val();
        if (result.value && password) {
          let userVm = {
            id: this.user,
            token: this.token,
            user: user.id,
            password: password
          };
          await (await this._authService.enableAdminUserAccount(userVm)).subscribe(async data => {
            Swal.fire({
              title: '<p class="text-white">Disabled!</p>',
              html: '<p class="text-white"> User\'s account has been disabled and the user will be logged out from Cardio Flourish application.</p>',
              icon: 'success',
              customClass: {
                confirmButton: "btn btn-success",
              },
              background: '#000',
              buttonsStyling: false
            })
            await this.getAllUsers();
          }, err => {
            let error = 'An error has occurred.'
            if (err.error.message) error = err.error.message;
            Swal.fire({
              title: '<p class="text-white">Failed!</p>',
              html: '<p class="text-white">' + error + ' User\'s account has not been disabled. Please Try Again.</p>',
              icon: 'error',
              customClass: {
                confirmButton: "btn btn-danger",
              },
              buttonsStyling: false,
              background: '#000',
            })
          })
        } else {
          Swal.fire({
            title: '<p class="text-white">Cancelled!</p>',
            html: '<p class="text-white"> User\'s account has not been disabled.</p>',
            icon: 'error',
            customClass: {
              confirmButton: "btn btn-info",
            },
            background: '#000',
            buttonsStyling: false
          })
        }
      })
    } else {
      Swal.fire({
        title: '<p class="text-white">Enable User Account. Are you sure?</p>',
        icon: 'warning',
        html: '<div class="form-group text-white">' +
          '<span data-notify="message"> This user\'s account will be enabled and the user will have access to Cardio Flourish app.</span>' +
          '<br> <br>' +
          '<label>"' + user.fullName + '"</label>' +
          '<br> <br>' +
          '<label>If so, enter your password</label>' +
          '<input id="input-field" type="password" class="form-control"/>' +
          '</div>',
        showCancelButton: true,
        customClass: {
          cancelButton: 'btn btn-danger',
          confirmButton: 'btn btn-success',
        },
        confirmButtonText: 'Yes, enable it!',
        background: '#000',
        buttonsStyling: false,
        preConfirm: () => {
          const password = $('#input-field').val();
          if (!password)
            Swal.showValidationMessage(
              'Password is required'
            )
        },
        allowOutsideClick: () => !Swal.isLoading()
      }).then(async (result: any) => {
        const password = $('#input-field').val();
        if (result.value && password) {
          let userVm = {
            id: this.user,
            token: this.token,
            user: user.id,
            password: password
          };
          await (await this._authService.enableAdminUserAccount(userVm)).subscribe(async data => {
            Swal.fire({
              title: '<p class="text-white">Enabled!</p>',
              html: '<p class="text-white"> User\'s account has been enabled and the user will be logged out from Cardio Flourish application.</p>',
              icon: 'success',
              customClass: {
                confirmButton: "btn btn-success",
              },
              buttonsStyling: false,
              background: '#000',
            })
            await this.getAllUsers();
          }, err => {
            console.log('err', err);
            let error = 'An error has occurred.'
            if (err.error.message) error = err.error.message;
            Swal.fire({
              title: '<p class="text-white">Failed!</p>',
              html: '<p class="text-white">' + error + ' User\'s account has not been enabled. Please Try Again.</p>',
              icon: 'error',
              customClass: {
                confirmButton: "btn btn-danger",
              },
              buttonsStyling: false,
              background: '#000',
            })
          })
        } else {
          Swal.fire({
            title: '<p class="text-white">Cancelled!</p>',
            html: '<p class="text-white"> User\'s account has not been enabled.</p>',
            icon: 'error',
            customClass: {
              confirmButton: "btn btn-info",
            },
            buttonsStyling: false,
            background: '#000',
          })
        }
      })
    }
  }

  async deleteUser(user: USER) {

    if (user.id == this.user) return;

    Swal.fire({
      title: '<p class="text-white">Delete User Account. Are you sure?</p>',
      icon: 'warning',
      html: '<div class="form-group text-white">' +
        '<span data-notify="message"> This user\'s account will be deleted and the user will no longer have access to Cardio Flourish app. All the data will be deleted and the account cannot be restored.</span>' +
        '<br> <br>' +
        '<label>"' + user.fullName + '"</label>' +
        '<br> <br>' +
        '<label>If so, enter your password</label>' +
        '<input id="input-field" type="password" class="form-control"/>' +
        '</div>',
      showCancelButton: true,
      customClass: {
        cancelButton: 'btn btn-danger',
        confirmButton: 'btn btn-success',
      },
      background: '#000',
      confirmButtonText: 'Yes, delete it!',
      buttonsStyling: false,
      preConfirm: () => {
        const password = $('#input-field').val();
        if (!password)
          Swal.showValidationMessage(
            'Password is required'
          )
      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then(async (result: any) => {
      const password = $('#input-field').val();
      if (result.value && password) {
        let userVm = {
          id: this.user,
          token: this.token,
          user: user.id,
          password: password
        };
        await (await this._authService.deleteAdminUserAccount(userVm)).subscribe(async data => {
          Swal.fire({
            title: '<p class="text-white">Deleted!</p>',
            html: '<p class="text-white"> User\'s account has been deleted and the user will be logged out from Cardio Flourish application.</p>',
            icon: 'success',
            customClass: {
              confirmButton: "btn btn-success",
            },
            background: '#000',
            buttonsStyling: false
          })
          await this.getAllUsers();
        }, err => {
          // console.log('err', err);
          let error = 'An error has occurred.'
          if (err.error.message) error = err.error.message;
          Swal.fire({
            title: '<p class="text-white">Failed!</p>',
            html: '<p class="text-white">' + error + ' User\'s account has not been deleted. Please Try Again.</p>',
            icon: 'error',
            customClass: {
              confirmButton: "btn btn-danger",
            },
            background: '#000',
            buttonsStyling: false
          })
        })
      } else {
        Swal.fire({
          title: '<p class="text-white">Cancelled!</p>',
          html: '<p class="text-white"> User\'s account has not been deleted.</p>',
          icon: 'error',
          customClass: {
            confirmButton: "btn btn-info",
          },
          background: '#252422',
          buttonsStyling: false
        })
      }
    })
  }

  ngOnDestroy() {
    this.subscriptionUser.unsubscribe();
  }
}
