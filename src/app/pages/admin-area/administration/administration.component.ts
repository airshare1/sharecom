import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AuthenticationService } from "providers/authentication.service";
import { ConfigService } from "providers/config/config.service";
import { Subscription } from "rxjs";
import Swal from "sweetalert2";

declare interface Menu {
  path: string;
  link: boolean;
  title: string;
  description: string;
  icon: string;
  disabled: boolean;
}

export const MenuItems: Menu[] = [
  {
    path: "/admin-user",
    icon: "fas fa-users",
    title: "Users",
    disabled: false,
    link: true,
    description: "Manage Cardio Flourish Users.",
  },
  // {
  //   path: "/admin-communications",
  //   icon: "fas fa-mail-bulk",
  //   title: "Communications",
  //   disabled: false,
  //   link: true,
  //   description: "Manage Cardio Flourish Communications.",
  // },
  // {
  //   path: "/admin-video-library",
  //   title: "Video Library",
  //   icon: "fas fa-photo-video",
  //   disabled: false,
  //   link: true,
  //   description: "Manage Cardio Flourish Video Library.",
  // },
  // {
  //   path: "NUTRITION_FILE_UPLOAD",
  //   title: "Nutrition File Upload",
  //   icon: "fas fa-utensils",
  //   disabled: false,
  //   link: false,
  //   description: "Upload Nutrition Information File.",
  // },
  // // Fit Camp
  // {
  //   path: "/all-fit-camps",
  //   title: "Fit Camps",
  //   icon: "fas fa-dumbbell",
  //   disabled: false,
  //   link: true,
  //   description: "Manage Cardio Flourish Fit Camps.",
  // },
];

@Component({
  selector: "app-administration",
  templateUrl: "./administration.component.html",
  styleUrls: ["./administration.component.css"],
})
export class AdministrationComponent implements OnInit, OnDestroy {
  Toast = Swal.mixin({
    toast: true,
    position: "bottom",
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });

  public menuItems: any[];

  admin: boolean = false;

  user = {
    id: "",
    token: "",
    name: "",
    surname: "",
    fullName: "",
    email: "",
    avatar: "",
  };

  totalUser: number = 0;
  totalTeam: number = 0;
  totalCompany: number = 0;

  private subscriptionUser: Subscription;

  constructor(
    private _authService: AuthenticationService,
    private readonly _configService: ConfigService,
    private modalService: NgbModal,
    private router: Router
  ) {}

  async ngOnInit() {
    this.menuItems = MenuItems.filter((menuItem) => menuItem);

    this.subscriptionUser = this._authService.authUserChange.subscribe(
      async (value) => {
        let user: any[] = value ? JSON.parse(value) : {};
        this.user.id = user["_id"];
        this.user.token = user["token"];
        this.user.fullName = user["name"] + " " + user["surname"];
        this.user.name = user["name"];
        this.user.surname = user["surname"];
        this.user.email = user["email"];
        this.user.avatar = this._configService.url + user["avatar"];

        if (user["role"] == "Admin") this.admin = true;
        else this.admin = false;

        if (!this.admin) this.router.navigate(["/dashboard"]);
      }
    );

    //await this.createClientStripeAccount();
  }

  // async createClientStripeAccount() {
  //   const userVm = {
  //     id: this.user.id,
  //     token: this.user.token,
  //     id_client: this.user.id,
  //   };

  //   await (
  //     await this._authService.createClientStripeAccount(userVm)
  //   ).subscribe(
  //     (newData) => {
  //       console.log("newData", newData);
  //     },
  //     (err) => {
  //       console.log("error");
  //     }
  //   );
  // }

  // initiateSystem() {
  //   this._authService.initiateSystem(this.user.token).subscribe(

  ngOnDestroy() {
    this.subscriptionUser.unsubscribe();
  }

  // async openModal(component: string) {
  //   if (component == "NUTRITION_FILE_UPLOAD") {
  //     const modalRef = this.modalService.open(NutritionFileUploadComponent, {
  //       size: "lg",
  //       backdrop: "static",
  //       centered: true,
  //     });
  //     modalRef.componentInstance.user = this.user.id;
  //     modalRef.componentInstance.token = this.user.token;
  //     modalRef.result.then(async (result) => {
  //       if (result == "success") {
  //         this.Toast.fire({
  //           icon: "success",
  //           title: "The nutrition file has been uploaded successfully.",
  //         });
  //       }
  //     });
  //   }
  // }
  
}
