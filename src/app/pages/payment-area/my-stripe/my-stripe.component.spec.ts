import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyStripeComponent } from './my-stripe.component';

describe('MyStripeComponent', () => {
  let component: MyStripeComponent;
  let fixture: ComponentFixture<MyStripeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyStripeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyStripeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
