import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthenticationService } from "providers/authentication.service";
import { ConfigService } from "providers/config/config.service";
import { Subscription } from "rxjs";

export class StripeAccountResponse {
  id: string;
  object: string;
  business_profile: {
    mcc: string;
    name: string;
    product_description: string;
    support_address: string;
    support_email: string;
    support_phone: string;
    support_url: string;
    url: string;
  };
  capabilities: {
    card_payments: string;
    transfers: string;
  };
  charges_enabled: boolean;
  controller: {
    type: string;
    is_controller: boolean;
  };
  country: string;
  created: number;
  default_currency: string;
  details_submitted: boolean;
  email: string;
  external_accounts: {
    object: string;
    data: string[];
    has_more: boolean;
    url: string;
  };
  future_requirements: {
    alternatives: string[];
    current_deadline: string;
    currently_due: string[];
    disabled_reason: string;
    errors: string[];
    eventually_due: string[];
    past_due: string[];
    pending_verification: [];
  };
  metadata: {};
  payouts_enabled: boolean;
  requirements: {
    alternatives: string[];
    current_deadline: string;
    currently_due: string[];
    disabled_reason: string[];
    errors: string[];
    eventually_due: string[];
    past_due: string[];
    pending_verification: [];
  };
  settings: {
    bacs_debit_payments: {};
    branding: {
      icon: string;
      logo: string;
      primary_color: string;
      secondary_color: null;
    };
    card_issuing: {
      tos_acceptance: {
        date: string;
        ip: null;
      };
    };
    card_payments: {
      decline_on: {
        avs_failure: boolean;
        cvc_failure: boolean;
      };
      statement_descriptor_prefix: string;
      statement_descriptor_prefix_kanji: string;
      statement_descriptor_prefix_kana: null;
    };
    dashboard: {
      display_name: string;
      timezone: string;
    };
    payments: {
      statement_descriptor: string;
      statement_descriptor_kana: string;
      statement_descriptor_kanji: string;
    };
    payouts: {
      debit_negative_balances: boolean;
      schedule: {
        delay_days: number;
        interval: string;
      };
      statement_descriptor: string;
    };
    sepa_debit_payments: {};
  };
  tos_acceptance: {
    date: string;
    ip: string;
    user_agent: null;
  };
  type: string;
}

@Component({
  selector: "app-my-stripe",
  templateUrl: "./my-stripe.component.html",
  styleUrls: ["./my-stripe.component.css"],
})
export class MyStripeComponent implements OnInit, OnDestroy {

  public menuItems: any[];

  coach: boolean = false;

  user = {
    id: "",
    token: "",
    name: "",
    surname: "",
    fullName: "",
    email: "",
    avatar: "",
  };

  stripeAccount: StripeAccountResponse;

  private subscriptionUser: Subscription;

  constructor(
    private _authService: AuthenticationService,
    private readonly _configService: ConfigService,
    private router: Router
  ) {}

  async ngOnInit() {
    this.subscriptionUser = this._authService.authUserChange.subscribe(
      async (value) => {
        let user: any[] = value ? JSON.parse(value) : {};
        this.user.id = user["_id"];
        this.user.token = user["token"];
        this.user.fullName = user["name"] + " " + user["surname"];
        this.user.name = user["name"];
        this.user.surname = user["surname"];
        this.user.email = user["email"];
        this.user.avatar = this._configService.url + user["avatar"];

        if (user["role"] == "Admin" || user["role"] == "Coach") this.coach = true;
        else this.coach = false;

        if (!this.coach) this.router.navigate(["/dashboard"]);
      }
    );

    await this.getClientStripeAccount();
  }

  async getClientStripeAccount() {
    const userVm = {
      id: this.user.id,
      token: this.user.token,
      id_client: this.user.id,
    };

    await (
      await this._authService.getClientStripeAccount(userVm)
    ).subscribe(
      (newData) => {
        // console.log("newData", newData);
        this.stripeAccount = newData;
        console.log('stripeAccount', this.stripeAccount);
      },
      (err) => {
        console.log("error");
      }
    );
  }

  // initiateSystem() {
  //   this._authService.initiateSystem(this.user.token).subscribe(

  ngOnDestroy() {
    this.subscriptionUser.unsubscribe();
  }
}
