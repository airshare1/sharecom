import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthenticationService } from "providers/authentication.service";
import { Subscription } from "rxjs";

export class StripeAccountLinksType {
  object: string;
  created: number;
  expires_at: number;
  url: string;
}

export class StripeAccountResponse {
  account_links: StripeAccountLinksType = null;
  connected: boolean = false;
  isThereASubscription: boolean = false;
  expires_at: string = "";
  status: string = "";
  payment_link: string = "";
  canceled: boolean = false;
  cancel_at_period_end: boolean = false;
}

declare interface Menu {
  link?: string;
  path?: string;
  title: string;
  description: string;
  icon: string;
  disabled: boolean;
}

export const MenuItems: Menu[] = [
  {
    link: "https://buy.stripe.com/test_9AQdSP4Fh5n5gBG7ss",
    icon: "fas fa-money",
    title: "Cardio Flourish Membership",
    disabled: false,
    description: "Join the Cardio Flourish Community.",
  },
  {
    path: "/my-stripe-account",
    icon: "fab fa-stripe",
    title: "My Stripe Account",
    disabled: false,
    description: "Manage your Stripe Account.",
  },
  // {
  //   path: "/all-client-programs",
  //   icon: "fas fa-layer-group",
  //   title: "All Programs",
  //   disabled: false,
  //   description: "Find All Client's Programs",
  // },
];

@Component({
  selector: "app-membership",
  templateUrl: "./membership.component.html",
  styleUrls: ["./membership.component.css"],
})
export class MembershipComponent implements OnInit {
  admin: boolean = false;
  userId: string;
  token: string;

  loading: boolean = true;
  loadingSecondary: boolean = false;

  newsubscription: boolean = false;

  StripeAccountURL: string;

  stripeResponse: StripeAccountResponse = new StripeAccountResponse();
  // stripeResponse = {
  //   account_links: null,
  //   connected: false,
  //   isThereASubscription: false,
  //   expires_at: "",
  //   cancel_at_period_end: false,
  // };

  isAccountConnected: boolean = false;

  public menuItems: any[];
  private subscriptionUser: Subscription;

  constructor(
    private _authService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  async ngOnInit() {
    this.menuItems = MenuItems.filter((menuItem) => menuItem);

    this.subscriptionUser = this._authService.authUserChange.subscribe(
      (value) => {
        let user: any[] = value ? JSON.parse(value) : {};
        this.userId = user["_id"];
        this.token = user["token"];

        if (user["role"] == "Admin")
          this.admin = true;
        else this.admin = false;

        if (!this.admin) this.router.navigate(["/dashboard"]);
      }
    );

    this.newsubscription = this.route.snapshot.paramMap.get("newsubscription")
      ? JSON.parse(this.route.snapshot.paramMap.get("newsubscription"))
      : false;

    if (!this.newsubscription)
      await this.getStripeAccountLinkAndMembershipLink();
    else await this.stripeUpdateUserSubscriptionInformation();
  }

  async getStripeAccountLinkAndMembershipLink() {
    // this.loading = true;

    const userVm = {
      id: this.userId,
      token: this.token,
    };

    await (
      await this._authService.getStripeAccountLinkAndMembershipLink(userVm)
    ).subscribe(
      (newData) => {
        this.stripeResponse = newData;
        if (!this.stripeResponse.connected) {
          this.StripeAccountURL = this.stripeResponse.account_links.url;
          this.isAccountConnected = false;
        } else {
          this.isAccountConnected = true;
        }
        this.loading = false;
      },
      (err) => {
        this.loading = false;
        console.log("error");
      }
    );
  }

  async stripeUpdateUserSubscriptionInformation() {
    this.loading = true;

    const userVm = {
      id: this.userId,
      token: this.token,
    };

    await (
      await this._authService.stripeUpdateUserSubscriptionInformation(userVm)
    ).subscribe(
      async (newData) => {
        await this.getStripeAccountLinkAndMembershipLink();
      },
      (err) => {
        console.log("error");
      }
    );
  }

  async stripeCancelSubscription() {
    this.loadingSecondary = true;

    const userVm = {
      id: this.userId,
      token: this.token,
    };

    await (
      await this._authService.stripeCancelSubscription(userVm)
    ).subscribe(
      async (newData) => {
        await this.getStripeAccountLinkAndMembershipLink();
        this.loadingSecondary = false;
      },
      (err) => {
        this.loadingSecondary = false;
        console.log("error");
      }
    );
  }

  async stripeReativateSubscription() {
    this.loadingSecondary = true;

    const userVm = {
      id: this.userId,
      token: this.token,
    };

    await (
      await this._authService.stripeReativateSubscription(userVm)
    ).subscribe(
      async (newData) => {
        await this.getStripeAccountLinkAndMembershipLink();
        this.loadingSecondary = false;
      },
      (err) => {
        this.loadingSecondary = false;
        console.log("error");
      }
    );
  }

  ngOnDestroy() {
    this.subscriptionUser.unsubscribe();
  }
}
