import { StoreProvider } from './store/store';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config/config.service';
import { map } from "rxjs/operators";
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

export const USER_NAME: string = 'currentUser';
export const ADMIN_TEAM_ID: string = 'admin_team_id';
export const TEAM_ID: string = 'team_id';
export const CHALLENGE_ID: string = 'challenge_id';
export const PROJECT_ID: string = 'project_id';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  // Variable for determining if a user is logged in or not
  private authStatus$ = new BehaviorSubject<boolean>(false);
  private user = new BehaviorSubject<string>('');
  private notifications = new BehaviorSubject<string>('');

  userID: string;
  tokenID: string;

  constructor(
    private http: HttpClient,
    private storage: StoreProvider,
    private router: Router,
    private modalService: NgbModal,
    private readonly _configService: ConfigService) {

    this.authUserChange.subscribe(value => {
      let user: any[] = value ? JSON.parse(value) : {}
      this.userID = user['_id'];
      this.tokenID = user['token'];
    })
  }

  async checkAuthorization() {

    const tokenVm = {
      id: this.userID,
      token: this.tokenID,
    };

    this.setIsAuthorized(false);
    await this.http.post<any>(this._configService.url + 'user/token', tokenVm)
      .subscribe(async user => {
        if (user && user.token) {
          // this.setToken(user.token);
          await this.storage.setItem('token', user.token);
          await this.storage.setItem('id', user.user._id);
          delete user.token;
          await this.storage.setItem('currentUser', JSON.stringify(user.user));
          this.setUserLogin(JSON.stringify(user.user));
          user.user.isAuthorized = true;
          // this.setUser(JSON.stringify(''));
          this.setIsAuthorized(true);
        }
        //return user;

        // if (user && user.token) {
        //   // if (await this.getIsItMasterLogin()) {
        //   //   // user.user.role = 'User';
        //   //   // user.user.privilege.companyAdministrator = false;
        //   // }
        //   this.setUserLogin(JSON.stringify(user.user));
        //   this.setIsAuthorized(true);
        // }
      }, async err => {
        console.log('err - token', err);
        await this.logout();
        this.router.navigate(['/login']);
      });
  }

  // An observable that will change when our user has logged in
  get authStatusChange() {
    return this.authStatus$.asObservable();
  }

  get authUserChange() {
    console.log(this.user);
    return this.user.asObservable();
  }

  get authUserNotifications() {
    return this.notifications.asObservable();
  }

  // Set the is authorised
  setIsAuthorized(value: boolean) {
    this.authStatus$.next(value);
  }

  // Get the value of the user authorisation
  isAuthorized() {
    return this.authStatus$.value;
  }

  async getUserProfile(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(
      this._configService.url + "user/getUserProfile",
      userVm
    );
  }
  
  // Set the user
  async setItem(key: string, item: string): Promise<void> {
    await this.storage.setItem(key, item);
  }

  // Get the user
  async getItem(key: string): Promise<any> {
    return await this.storage.getItem(key);
  }

  // Clear the user
  async clearItem(key: string) {
    return await this.storage.remove(key);
  }

  setUserLogin(user: any): void {
    this.user.next(user);
  }

  setNotification(notifications: any): void {
    this.notifications.next(notifications);
  }

  async setAdminTeam(team: string): Promise<void> {
    await this.storage.setItem(ADMIN_TEAM_ID, team);
  }

  async getAdminTeam(): Promise<string> {
    return await this.storage.getItem(ADMIN_TEAM_ID);
  }

  async setTeam(team: string): Promise<void> {
    await this.storage.setItem(TEAM_ID, team);
  }

  async getTeam(): Promise<string> {
    return await this.storage.getItem(TEAM_ID);
  }

  async setChallenge(challenge: string): Promise<void> {
    await this.storage.setItem(CHALLENGE_ID, challenge);
  }

  async getChallenge(): Promise<string> {
    return await this.storage.getItem(CHALLENGE_ID);
  }

  // Get the user
  getUser(): any {
    return this.user.value;
  }

  // Clear the user
  async clearUser() {
    return await this.storage.remove(USER_NAME);
  }

  getAPI() {
    return this.http.get<any>(this._configService.url);
  }

  async login(loginVm) {
    this.setIsAuthorized(false);
    return await this.http.post<any>(this._configService.url + 'user/login', loginVm)
      .pipe(map(async user => {
        if (user && user.token) {
          // this.setToken(user.token);
          await this.storage.setItem('token', user.token);
          await this.storage.setItem('id', user.user._id);
          delete user.token;
          await await this.storage.setItem('currentUser', JSON.stringify(user.user));
          user.user.isAuthorized = true;
          // this.setUser(JSON.stringify(''));
          this.router.navigate(['/challenge-view-list']);
          this.setIsAuthorized(true);
        }
        return user;
      }));
  }

  async token(tokenVm) {
    this.setIsAuthorized(false);
    return await this.http.post<any>(this._configService.url + 'user/token', tokenVm)
      .pipe(map(async user => {
        if (user && user.token) {
          await this.storage.setItem('token', user.token);
          await this.storage.setItem('id', user.user._id);
          delete user.token;
          await this.storage.setItem('currentUser', JSON.stringify(user.user));
          user.user.isAuthorized = true;
          this.setUserLogin(JSON.stringify(user.user));
          // this.setUser(JSON.stringify(''));
          this.router.navigate(['/dashboard']);
          this.setIsAuthorized(true);
        }
        return user;
      }));
  }

  async EditUser(userVm: any) {
    return await this.http.post<any>(this._configService.url + 'user/editUser/', userVm)
      .pipe(map(async user => {
        if (user && user.token) {
          await this.storage.setItem('token', user.token);
          await this.storage.setItem('id', user.user._id);
          delete user.token;
          await this.storage.setItem('currentUser', JSON.stringify(user.user));
          this.setUserLogin(JSON.stringify(user.user));
          user.user.isAuthorized = true;
          // this.setUser('');
        }
        return user;
      }));
  }

  async joinSuggestedTeam(userVm: any) {
    return await this.http.post<any>(this._configService.url + 'team/joinSuggestedTeam/', userVm)
      .pipe(map(async user => {
        if (user && user.token) {
          await this.storage.setItem('token', user.token);
          await this.storage.setItem('id', user.user._id);
          delete user.token;
          await this.storage.setItem('currentUser', JSON.stringify(user.user));
          this.setUserLogin(JSON.stringify(user.user));
          user.user.isAuthorized = true;
          // this.setUser('');
        }
        return user;
      }));
  }

  async joinChallenge(userVm: any) {
    return await this.http.post<any>(this._configService.url + 'challenge/joinChallenge/', userVm)
      .pipe(map(async user => {
        if (user && user.token) {
          await this.storage.setItem('token', user.token);
          await this.storage.setItem('id', user.user._id);
          delete user.token;
          await this.storage.setItem('currentUser', JSON.stringify(user.user));
          this.setUserLogin(JSON.stringify(user.user));
          user.user.isAuthorized = true;
          // this.setUser('');
        }
        return user;
      }));
  }

  async leaveChallenge(userVm: any) {
    return await this.http.post<any>(this._configService.url + 'challenge/leaveChallenge/', userVm)
      .pipe(map(async user => {
        if (user && user.token) {
          await this.storage.setItem('token', user.token);
          await this.storage.setItem('id', user.user._id);
          delete user.token;
          await this.storage.setItem('currentUser', JSON.stringify(user.user));
          this.setUserLogin(JSON.stringify(user.user));
          user.user.isAuthorized = true;
          // this.setUser('');
        }
        return user;
      }));
  }

  async joinTeam(userVm: any) {
    return await this.http.post<any>(this._configService.url + 'team/joinTeam/', userVm)
      .pipe(map(async user => {
        if (user && user.token) {
          await this.storage.setItem('token', user.token);
          await this.storage.setItem('id', user.user._id);
          delete user.token;
          await this.storage.setItem('currentUser', JSON.stringify(user.user));
          this.setUserLogin(JSON.stringify(user.user));
          user.user.isAuthorized = true;
          // this.setUser('');
        }
        return user;
      }));
  }

  async leaveTeam(userVm: any) {
    return await this.http.post<any>(this._configService.url + 'team/leaveTeam/', userVm)
      .pipe(map(async user => {
        if (user && user.token) {
          await this.storage.setItem('token', user.token);
          await this.storage.setItem('id', user.user._id);
          delete user.token;
          await this.storage.setItem('currentUser', JSON.stringify(user.user));
          this.setUserLogin(JSON.stringify(user.user));
          user.user.isAuthorized = true;
          // this.setUser('');
        }
        return user;
      }));
  }

  async tokenDashboard(tokenVm) {
    this.setIsAuthorized(false);
    return await this.http.post<any>(this._configService.url + 'user/token', tokenVm)
      .pipe(map(async user => {
        if (user && user.token) {
          // this.setToken(user.token);
          await this.storage.setItem('token', user.token);
          await this.storage.setItem('id', user.user._id);
          delete user.token;
          await this.storage.setItem('currentUser', JSON.stringify(user.user));
          this.setUserLogin(JSON.stringify(user.user));
          user.user.isAuthorized = true;
          // this.setUser(JSON.stringify(''));
          this.setIsAuthorized(true);
        }
        return user;
      }));
  }

  async register(register: any) {
    console.log('register', register);
    return await this.http.post<any>(this._configService.url + 'user/register', register)
      .pipe(map(data => {
      }));
  }

  async createCalendarEvent(calendarVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'calendar/createEvent', calendarVm)
      .pipe(map(data => {
      }));
  }

  async getAllCompaniesEvents(ideaVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'calendar/getAllCompaniesEvents', ideaVm);
  }

  async getAllChallengeEvents(ideaVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'calendar/getAllChallengeEvents', ideaVm);
  }

  async getAllTeamEvents(ideaVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'calendar/getAllTeamEvents', ideaVm);
  }

  async createIdea(ideaVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'idea/create', ideaVm)
      .pipe(map(data => {
      }));
  }

  async editIdea(ideaVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'idea/edit', ideaVm)
      .pipe(map(data => {
      }));
  }

  async deleteIdea(ideaVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'idea/delete', ideaVm)
      .pipe(map(data => {
      }));
  }

  async flaggingIdea(ideaVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'idea/flagging', ideaVm)
      .pipe(map(data => {
      }));
  }

  async ratingIdeas(ideaVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'idea/ratingIdeas', ideaVm);
  }

  async rankingIdeas(ideaVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'idea/rankingIdeas', ideaVm);
  }

  async getAllRatedIdeas(ideaVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'idea/getAllRatedIdeas', ideaVm);
  }

  async createFaq(faq: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'faq/create', faq)
      .pipe(map(data => {
      }));
  }

  async sendEmail(sendEmailVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'newsletter/sendEmail', sendEmailVm)
      .pipe(map(data => {
      }));
  }

  async getAllEmails(sendEmailVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'newsletter/getAllEmails', sendEmailVm);
  }

  async deleteEmail(sendEmailVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'newsletter/deleteContactUs', sendEmailVm);
  }

  async createTeam(teamVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'team/create', teamVm)
      .pipe(map(data => {
      }));
  }

  async createChallenge(challengeVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'challenge/create', challengeVm)
      .pipe(map(data => {
      }));
  }

  async createComment(commentVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'challenge/comment', commentVm)
      .pipe(map(data => {
      }));
  }

  async createChallengeUploadMultimedia(formData: FormData) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'multimedia/createChallengeUploadMultimedia', formData)
  }

  async uploadMultimedia(formData: FormData) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'multimedia/uploadMultimedia', formData)
  }

  async editTeam(teamVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'team/editTeam', teamVm)
      .pipe(map(data => {
      }));
  }

  async editChallenge(challengeVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'challenge/editChallenge', challengeVm)
      .pipe(map(data => {
      }));
  }

  async createCompany(companyVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'company/create', companyVm)
      .pipe(map(data => {
      }));
  }

  async editCompany(companyVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'company/editCompany', companyVm)
      .pipe(map(data => {
      }));
  }

  async createCategory(categoryVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'category/create', categoryVm)
      .pipe(map(data => {
      }));
  }

  async editCategory(categoryVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'category/editcategory', categoryVm)
      .pipe(map(data => {
      }));
  }

  async editUserRole(userVm: any) {
    await this.checkAuthorization();
    return await this.http.put<any>(this._configService.url + 'user/role/', userVm)
      .pipe(map(data => {
      }));
  }

  async editUserPrivilege(userVm: any) {
    await this.checkAuthorization();
    return await this.http.put<any>(this._configService.url + 'user/privilege/', userVm)
      .pipe(map(data => {
      }));
  }

  async addImage(formData: FormData) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'multimedia/addImage', formData);
  }

  async SystemProfilePicture(id: string, userVm: any) {
    await this.checkAuthorization();
    return await this.http.put<any>(this._configService.url + 'user/SystemProfilePicture/' + id, { userVm })
      .pipe(map(async user => {
        if (user && user.token) {
          // this.setToken(user.token);
          delete user.token;
          await this.storage.setItem('currentUser', JSON.stringify(user.user));
          this.setUserLogin(JSON.stringify(user.user));
          user.user.isAuthorized = true;
          // this.setUser('');
        }
        return user;
      }));
  }

  async EditPassword(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'user/password', userVm)
      .pipe(map(data => {
      }));
  }

  async EditProfilePicture(id: string, formData: FormData) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'multimedia/profilepicture/' + id, formData)
      .pipe(map(async user => {
        if (user && user.token) {
          delete user.token;
          await this.storage.setItem('currentUser', JSON.stringify(user.user));
          this.setUserLogin(JSON.stringify(user.user));
          user.user.isAuthorized = true;
          // this.setUser('');
        }
        return user;
      }));
  }

  async getAllContactUsMessage(contactUsVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'newsletter/contactUs', contactUsVm);
  }

  async getAllNewsletter(newsletterVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'newsletter/newsletter', newsletterVm);
  }

  async getAllFaq() {
    await this.checkAuthorization();
    return await this.http.get(this._configService.url + 'faq');
  }

  async getAdminAllUsers(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'user/getAdminAllUsers', userVm);
  }

  async getCompanyAllUsers(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'user/getCompanyAllUsers', userVm);
  }

  async getAdminAllTeams(teamVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'team/getAdminAllTeams', teamVm);
  }

  async getAllMyTeams(teamVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'team/getAllMyTeams', teamVm);
  }

  async getCompanyAllTeams(teamVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'team/getCompanyAllTeams', teamVm);
  }

  async getAllChallenges(challengeVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'challenge/getAll', challengeVm);
  }

  async getAllCompanyChallenges(challengeVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'challenge/getAllCompanyChallenges', challengeVm);
  }

  async getAllCompanyPreviousChallenges(challengeVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'challenge/getAllCompanyPreviousChallenges', challengeVm);
  }

  async getAllAdminCompanyChallenges(challengeVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'challenge/getAllAdminCompanyChallenges', challengeVm);
  }

  async getMyChallenges(challengeVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'challenge/getMyChallenges', challengeVm);
  }

  async getPreviousMyChallenges(challengeVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'challenge/getPreviousMyChallenges', challengeVm);
  }

  async getAllCompanies(companyVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'company/getAll', companyVm);
  }

  async getAllCompaniesAdmin(companyVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'company/getAllAdmin', companyVm);
  }

  async getAdminCompanyInfo(companyVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'company/getAdminCompanyInfo', companyVm);
  }

  async getAllCategories(categoryVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'category/getAll', categoryVm);
  }

  async getAllCategoriesByType(categoryVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'category/getAllByType', categoryVm);
  }

  async getAdminTeamProfile(teamVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'team/getAdminTeamProfile', teamVm);
  }

  async getTeamProfile(teamVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'team/getTeamProfile', teamVm);
  }

  async getChallengeProfile(challengeVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'challenge/getChallengeProfile', challengeVm);
  }

  async getEvent(challengeVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'calendar/getEvent', challengeVm);
  }

  async getSuggestedTeam(teamVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'team/getSuggestedTeam', teamVm);
  }

  async getAllMessages(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'messages/messages', userVm);
  }

  async getAllConversarionMessages(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'messages/conversation', userVm);
  }

  async sendChatMessage(messageVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'messages/create', messageVm)
      .pipe(map(data => {
      }));
  }

  async sendNotification(notification: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'notifications/notify', notification)
      .pipe(map(data => {
      }));
  }

  async getAllNotifications(notificationVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'notifications/getAllNotifications', notificationVm);
  }

  async getAllIdeas(ideaVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'idea/getAll', ideaVm);
  }

  async getAllFlaggedIdeas(ideaVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'idea/getAllFlaggedIdeas', ideaVm);
  }

  async getNavbarNotifications(notificationVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'notifications/getNavbarNotifications', notificationVm)
      .pipe(map(async notifications => {
        this.setNotification(JSON.stringify(notifications));
      }, err => {
        this.setNotification('');
      }));
  }

  async markAllNotificationsAsRead(notificationVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'notifications/markAllNotificationsAsRead', notificationVm);
  }

  async issueNewVerificationEmail(verificationEmailVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'verificationGeneration/issueNewVerificationEmail', verificationEmailVm);
  }

  async emailVerification(verificationEmailVm: any) {
    return await this.http.post<any>(this._configService.url + 'verificationGeneration/emailVerification', verificationEmailVm);
  }

  async markNotification(notificationVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'notifications/markNotification', notificationVm);
  }

  async forgotPassword(forgotPasswordVm: any) {
    return await this.http.post<any>(this._configService.url + 'verificationGeneration/newPasswordRequestCreation', forgotPasswordVm);
  }

  async resetPassword(forgotPasswordVm: any) {
    return await this.http.post<any>(this._configService.url + 'verificationGeneration/resetPassword', forgotPasswordVm);
  }

  async checkPasswordExpiration(forgotPasswordVm: any) {
    return await this.http.post<any>(this._configService.url + 'verificationGeneration/checkPasswordExpiration', forgotPasswordVm);
  }

  async logoutNotification(user: string) {
    return await this.http.put<any>(this._configService.url + 'user/logout/' + user, {})
      .pipe(map(data => {
      }));
  }

  async download(multimediaVm: any) {
    await this.checkAuthorization();
    return await this.http.post(this._configService.url + 'multimedia/download/', multimediaVm, { responseType: 'blob' });
  }

  async deleteFaq(faqVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'faq/deleteFaq', faqVm);
  }

  async deleteContactUsMessage(contactUsMessageVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'newsletter/deleteContactUs', contactUsMessageVm);
  }

  async deleteUser(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'user/deleteUser', userVm);
  }

  async deleteUserAccount(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'user/deleteUserAccount', userVm);
  }

  async deleteAdminUserAccount(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'user/deleteAdminUserAccount', userVm);
  }

  async deleteAdminTeam(teamVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'team/deleteAdminTeam', teamVm);
  }

  async deleteChallenge(challengeVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'challenge/deleteChallenge', challengeVm);
  }

    async deleteMultimedia(multimediaVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'multimedia/deleteMultimedia', multimediaVm);
  }

  async deleteAdminCompany(companyVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'company/deleteAdminCompany', companyVm);
  }

  async deleteAdminEvent(companyVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'calendar/deleteAdminEvent', companyVm);
  }

  async deleteAdminCategory(categoryVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'category/deleteAdminCategory', categoryVm);
  }

  async enableAdminUserAccount(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'user/enableAdminUserAccount', userVm);
  }

  async activateAdminTeam(teamVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'team/activateAdminTeam', teamVm);
  }

  async activateChallenge(teamVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'challenge/activateChallenge', teamVm);
  }

  async activateAdminCompany(companyVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'company/activateAdminCompany', companyVm);
  }

  async changeAdminUserRole(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'user/changeAdminUserRole', userVm);
  }

  async changeUserRole(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'user/changeUserRole', userVm);
  }

  async changeUserCompany(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'user/changeUserCompany', userVm);
  }

  async deleteConversation(messageVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'messages/deleteconversation', messageVm);
  }

  async logout() {
    this.modalService.dismissAll();
    await this.setIsAuthorized(false);
    // await this.clearToken();
    await this.clearUser();
    let firebaseToken = await this.storage.getItem('firebaseToken');
    await this.storage.clear();
    await this.storage.setItem('firebaseToken', firebaseToken);
    await this.setUserLogin(JSON.stringify(''));
  }

  async uploadPhotos(formData: FormData) {
    await this.checkAuthorization();
    return await this.http
      .post<any>(this._configService.url + "multimedia/uploadPhotos", formData)
      .pipe(
        map(async (user) => {
          if (user && user.token) {
            await this.storage.setItem("token", user.token);
            await this.storage.setItem("id", user.user._id);
            delete user.token;
            await this.storage.setItem(
              "currentUser",
              JSON.stringify(user.user)
            );
            this.setUserLogin(JSON.stringify(user.user));
            user.user.isAuthorized = true;
          }
          return user;
        })
      );
  }

  async uploadProfilePicture(formData: FormData) {
    await this.checkAuthorization();
    return await this.http
      .post<any>(
        this._configService.url + "multimedia/uploadProfilePicture",
        formData
      )
      .pipe(
        map(async (user) => {
          if (user && user.token) {
            await this.storage.setItem("token", user.token);
            await this.storage.setItem("id", user.user._id);
            delete user.token;
            await this.storage.setItem(
              "currentUser",
              JSON.stringify(user.user)
            );
            this.setUserLogin(JSON.stringify(user.user));
            user.user.isAuthorized = true;
          }
          return user;
        })
      );
  }

  async EditUserBio(userVm: any) {
    return await this.http
      .post<any>(this._configService.url + "user/editUserBio/", userVm)
      .pipe(
        map(async (user) => {
          if (user && user.token) {
            await this.storage.setItem("token", user.token);
            await this.storage.setItem("id", user.user._id);
            delete user.token;
            await this.storage.setItem(
              "currentUser",
              JSON.stringify(user.user)
            );
            this.setUserLogin(JSON.stringify(user.user));
            user.user.isAuthorized = true;
          }
          return user;
        })
      );
  }

  async resetProfilePicture(userVm: any) {
    await this.checkAuthorization();
    return await this.http
      .post<any>(this._configService.url + "user/resetProfilePicture", userVm)
      .pipe(
        map(async (user) => {
          if (user && user.token) {
            await this.storage.setItem("token", user.token);
            await this.storage.setItem("id", user.user._id);
            delete user.token;
            await this.storage.setItem(
              "currentUser",
              JSON.stringify(user.user)
            );
            this.setUserLogin(JSON.stringify(user.user));
            user.user.isAuthorized = true;
          }
          return user;
        })
      );
  }

  async setProject(project: string): Promise<void> {
    await this.storage.setItem(PROJECT_ID, project);
  }

  async initiateChallengeImplementation(challengeVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'challenge/initiateChallengeImplementation', challengeVm);
  }

  async createEmployeeProfileFit(teamVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'employee-profile-fit/createEmployeeProfileFit', teamVm);
  }

  async signTeamCharter(teamCharterVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'teamCharter/signTeamCharter', teamCharterVm)
      .pipe(map(data => {
      }));
  }

  async saveTeamCharter(teamCharterVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'teamCharter/saveTeamCharter', teamCharterVm)
      .pipe(map(data => {
      }));
  }

  async getTeamsCharter(teamCharterVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'teamCharter/getTeamsCharter', teamCharterVm);
  }

  async createProject(projectVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'project/create', projectVm)
      .pipe(map(data => {
      }));
  }

  async editProject(projectVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'project/editProject', projectVm)
      .pipe(map(data => {
      }));
  }

  async getProject(): Promise<string> {
    return await this.storage.getItem(PROJECT_ID);
  }

  async getProjectProfile(projectVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'project/getProjectProfile', projectVm);
  }

  async deleteProject(projectVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'project/deleteProject', projectVm);
  }

  async createPost(postVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'post/create', postVm)
      .pipe(map(data => {
      }));
  }

  async getAllProjectEvents(calendarVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'calendar/getAllProjectEvents', calendarVm);
  }

  async updateProject(projectVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(this._configService.url + 'project/updateProject', projectVm)
      .pipe(map(data => {
      }));
  }

  async createClientStripeAccount(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(
      this._configService.url + "payment/createClientStripeAccount",
      userVm
    );
  }

  async getClientStripeAccount(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(
      this._configService.url + "payment/getClientStripeAccount",
      userVm
    );
  }

  async getClientStripePaymentLink(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(
      this._configService.url + "payment/getClientStripePaymentLink",
      userVm
    );
  }

  async getStripeAccountLinkAndMembershipLink(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(
      this._configService.url + "payment/getStripeAccountLinkAndMembershipLink",
      userVm
    );
  }

  async stripeCancelSubscription(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(
      this._configService.url + "payment/stripeCancelSubscription",
      userVm
    );
  }

  async stripeUpdateUserSubscriptionInformation(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(
      this._configService.url +
        "payment/stripeUpdateUserSubscriptionInformation",
      userVm
    );
  }

  async stripeReativateSubscription(userVm: any) {
    await this.checkAuthorization();
    return await this.http.post<any>(
      this._configService.url + "payment/stripeReativateSubscription",
      userVm
    );
  }
}