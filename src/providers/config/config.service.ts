import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor() { }

  // public url = 'https://template.ie/api/'; //Replace template.ie with the domain name of the deployed API
  public url = 'http://localhost:8088/'; //Local Deployment
}
