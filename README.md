# Template Company Name

This template project was created using **[Paper Dashboard PRO Angular](https://www.creative-tim.com/product/paper-dashboard-pro-angular/)** (PDPA) - see below for more details. It was then modified by removing several of the PDPA directories and adding a basic web application with an admin-area, an app-area with user registration, login, userpage, about-us and contact,  a dashboard and a multimedia-area. It is configured to work with the NESTJS, MongoDB API https://gitlab.com/sri-ait-ie/ei/iv/templates/nestjsmongoapi/. The configuration file is in /src/providers/config folder.

## Terminal Commands

1. Install NodeJs from [NodeJs Official Page](https://nodejs.org/en).
2. Open Terminal
3. Go to your file project
4. Run in terminal: ``npm install -g @angular/cli``
5. Then: ``npm install``
6. And: ``ng serve``
7. Navigate to: [http://localhost:4200/](http://localhost:4200/)

# Licenses/Purchases Required

### Fonts

[https://fontawesome.com/](https://fontawesome.com/ "https://fontawesome.com/")

[https://www.fonts.com/content/microsoft-typography](https://www.fonts.com/content/microsoft-typography "https://www.fonts.com/content/microsoft-typography")

### Google Account

The Google Account ([mlstracemaster@gmail.com](mailto:mlstracemaster@gmail.com "mailto:mlstracemaster@gmail.com")) was created to provide access to the Firebase Platform Services ([https://firebase.google.com/](https://firebase.google.com/ "https://firebase.google.com/")). The MLS Trace Master Application uses Firebase Cloud Messaging (FCM). The FCM is a cross-platform solution for messages and notifications for Android, iOS, and web applications, which can be used at no cost.

Current Android Package Name and iOS Bundle ID: `"ie.tracemaster.app"`

### Send Grid Account

SendGrid is a cloud-based SMTP provider that allows you to send email without having to maintain email servers. SendGrid manages all of the technical details, from scaling the infrastructure to ISP outreach and reputation monitoring to whitelist services and real-time analytics.

A SendGrid Account was created by MLS using the Google Account ([mlstracemaster@gmail.com](mailto:mlstracemaster@gmail.com "mailto:mlstracemaster@gmail.com")) but this account has been suspended.

Digi will need to create their own SendGrid Account. You will create templates which will have ids. The software must be updated with these ids. E.g. Replace template_id in the following with your sendEmailVerificationLink template_id.

```
    async sendEmailVerificationLink(params: PasswordRequestLinkParams, origin: string): Promise<any> {
        const { name, email, id } = params;

        const sgMail = require('@sendgrid/mail');
        sgMail.setApiKey(SENDGRID_API_KEY);

        const msg = {
            to: email,
            from: {
                email: 'support@digi.ie',
                name: 'DIGI System'
            },
            template_id: 'd-5162db9d60f943ed9fa1e5024461a793', 
            dynamic_template_data: {
                name: name,
                email: email,
                passwordLink: id,
                origin: origin
            }
        };
        sgMail.send(msg);
        return msg;
    }
```

### Cloudemersive Account

This account is used for converting word to pdf in Report Generation. Note report generation is not included in this template.

[https://account.cloudmersive.com/login](https://account.cloudmersive.com/login "https://account.cloudmersive.com/login")

The login to this account is made using the 'Sign In With Google” feature.

# Deployment - First Application

On the development server

```bash
# Build the application
IMPORTANT. Before you build, update file providers -> config -> config.service.ts with the url of API on the deployment server. 

> ng build


# Copy the application to the deployment server
In a command shell at the folder where the private key for the deployment server (ip address:xxx.xxx.xxx.xxx) is stored, 
pathto is your path to the /web folder and id_rsa is your private key.
substitute xxx.xxx.xxx.xxx and then run the command

> scp -i "id_rsa" -r pathto/web/dist/ root@xxx.xxx.xxx.xxx:~/dist/

# SSH to server
> ssh -i "id_rsa" root@xxx.xxx.xxx.xxx

```

On the deployment server

```bash
# Install new version of nginx
$ sudo service nginx stop  
$ apt-get --purge remove nginx-*  
$ sudo apt-get update  
$ sudo apt-get install nginx  

# Copy Application to nginx
$ sudo cp -r ./dist/* /var/www/html/

# Start nginx
$ sudo service nginx start

```

# Secure website with HTTPS

Secure according to company policies and certificate provider.
The following is one example of how to secure using Letsencrypt as certificate authority.
Follow the instructions on https://certbot.eff.org/
Choose nginx and Ubuntu 20 OS
When prompted enter all domain names for your server e.g. example.com, www.example.com

## Add additional security

Edit the /etc/nginx/sites-enabled/default file as follows:

### Add /api path

In the server { that contains listen on 443, add the following text

location /api{
rewrite ^/api(/.*)$ $1 break;
try_files $uri $uri/ @proxy;
}

location @proxy {
proxy_pass http://127.0.0.1:8080$uri;
}

### Redirect from unsecure IP address to the secure hostname

Add new server by adding the following text where xxx.xxx.xxx.xxx is the IP address of the server and hostname is the domain name of the server e.g. example.com.

server {
if ($host = xxx.xxx.xxx.xxx) {
return 301 https://hostname$request_uri;
} # managed by Certbot
listen 80 ;
listen [::]:80 ;
server_name xxx.xxx.xxx.xxx;
return 404; # managed by Certbot
}

# Kiosk Mode

Create chrome shortcut.

Close all instances of chrome.

Right click on chrome shortcut -> click properties and update the following

In Target:

"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --profile-directory="Default"  --kiosk --kiosk-printing "http://hostname/"

In Start in:

"C:\Program Files (x86)\Google\Chrome\Application"

# Deployment - Update Application

On the development server

```bash
# NB! Update configuration file src->providers -> config -> config.service.ts so that web points to api of the deployment server

> ng build  

# Copy the application to the deployment server
> scp -i "id_rsa" -r pathto/web/dist/ root@xxx.xxx.xxx.xxx:~/dist/  

# SSH to server  
> ssh -i "id_rsa" root@xxx.xxx.xxx.xxx  
 
```

On the deployment server

```bash
# Copy Application to nginx  
$ sudo cp -r ./dist/* /var/www/html/  

# Restart nginx  
$ sudo service nginx restart  


```

# Development

```bash
# Clone the application   
> git clone https://gitlab.com/sri-ait-ie/ei/innovation-partnerships/mls/uildts/mls-sri-demo/web.git

# cd to web folder and install the node modules  
> cd web
> npm install   

# Start the application 
> ng serve

```

Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.
For example, to create a new component named 'new_component' in a main section named 'main_area' and a subsection named 'subsection_area', run the following command:
ng g component pages\main_area\subsection_area\new_component --module=layouts\admin-layout\admin-layout.module.ts

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# [Paper Dashboard Pro Angular](https://demos.creative-tim.com/paper-dashboard-pro-angular) [![Tweet](https://img.shields.io/twitter/url/http/shields.io.svg?style=social&logo=twitter)](https://twitter.com/home?status=Paper%20Dashboard%20PRO%20Angular%20by%20Creative%20Tim%20https%3A//demos.creative-tim.com/paper-dashboard-pro-angular%20via%20%40CreativeTim)

![version](https://img.shields.io/badge/version-1.4.0-blue.svg) [![GitHub issues open](https://img.shields.io/github/issues/creativetimofficial/ct-paper-dashboard-pro-angular.svg?maxAge=2592000)](https://github.com/creativetimofficial/ct-paper-dashboard-pro-angular/issues?q=is%3Aopen+is%3Aissue) [![GitHub closed issues](https://img.shields.io/github/issues-closed-raw/creativetimofficial/ct-paper-dashboard-pro-angular.svg?maxAge=259200)](https://github.com/creativetimofficial/ct-paper-dashboard-pro-angular/issues?q=is%3Aissue+is%3Aclosed)  [![Chat](https://img.shields.io/badge/chat-on%20discord-7289da.svg)](https://discord.gg/E4aHAQy)

![Product Gif](https://media.giphy.com/media/5WJf1Jp5C0Ve1I2nne/giphy.gif)

**[Paper Dashboard PRO Angular](https://www.creative-tim.com/product/paper-dashboard-pro-angular/)** is the premium version for the [Paper Dashboard Angular](https://www.creative-tim.com/product/paper-dashboard-angular), which is available for free download.

During the development of this dashboard, we have used many existing resources from awesome developers. We want to thank them for providing their tools open source:

* [Robert McIntosh](https://github.com/mouse0270) for the notification system
* [Chartist](https://gionkunz.github.io/chartist-js/) for the wonderful charts
* [Tristan Edwards](https://twitter.com/t4t5) for the [Sweet Alert](http://limonte.github.io/sweetalert2/)
* [Eonasdan](https://github.com/Eonasdan) for the [DateTimPicker](https://eonasdan.github.io/bootstrap-datetimepicker/)
* Kirill Lebedev for [jVector Maps](http://jvectormap.com/)
* [Vincent Gabriel](https://twitter.com/gabrielva) for the [Bootstrap Wizard](https://github.com/VinceG/twitter-bootstrap-wizard)
* [FullCalendar.io](https://fullcalendar.io/) for the awesome Calendar

Let us know your thoughts below. And good luck with development!

## Table of Contents

* [Versions](#versions)
* [Demo](#demo)
* [Quick Start](#quick-start)
* [Terminal Commands](#terminal-commands)
* [Documentation](#documentation)
* [File Structure](#file-structure)
* [Browser Support](#browser-support)
* [Resources](#resources)
* [Reporting Issues](#reporting-issues)
* [Technical Support or Questions](#technical-support-or-questions)
* [Licensing](#licensing)
* [Useful Links](#useful-links)

## Quick start

Quick start options:

- Buy from [Creative Tim](https://www.creative-tim.com/product/paper-dashboard-pro-angular)

## Documentation

The documentation for the Paper Dashboard Pro Angular is hosted at [Creative Tim]()  [website](https://demos.creative-tim.com/paper-dashboard-pro-angular/documentation/tutorial).

## Original File Structure of Paper Dashboard Pro Angular

Within the original download you'll find the following directories and files:

```
paper-dashboard-pro-angular/
├── CHANGELOG.md
├── README.md
├── angular.json
├── documentation
│   ├── css
│   ├── js
│   └── tutorial-components.html
├── e2e
├── karma.conf.js
├── package.json
├── protractor.conf.js
├── src
│   ├── app
│   │   ├── app.component.html
│   │   ├── app.component.spec.ts
│   │   ├── app.component.ts
│   │   ├── app.module.ts
│   │   ├── app.routing.ts
│   │   ├── calendar
│   │   │   ├── calendar.component.html
│   │   │   ├── calendar.component.ts
│   │   │   ├── calendar.module.ts
│   │   │   └── calendar.routing.ts
│   │   ├── charts
│   │   │   ├── charts.component.html
│   │   │   ├── charts.component.ts
│   │   │   ├── charts.module.ts
│   │   │   └── charts.routing.ts
│   │   ├── components
│   │   │   ├── buttons
│   │   │   │   ├── buttons.component.html
│   │   │   │   └── buttons.component.ts
│   │   │   ├── components.module.ts
│   │   │   ├── components.routing.ts
│   │   │   ├── grid
│   │   │   │   ├── grid.component.html
│   │   │   │   └── grid.component.ts
│   │   │   ├── icons
│   │   │   │   ├── icons.component.html
│   │   │   │   └── icons.component.ts
│   │   │   ├── notifications
│   │   │   │   ├── notifications.component.html
│   │   │   │   └── notifications.component.ts
│   │   │   ├── panels
│   │   │   │   ├── panels.component.html
│   │   │   │   └── panels.component.ts
│   │   │   ├── sweetalert
│   │   │   │   ├── sweetalert.component.html
│   │   │   │   └── sweetalert.component.ts
│   │   │   └── typography
│   │   │       ├── typography.component.html
│   │   │       └── typography.component.ts
│   │   ├── dashboard
│   │   │   ├── dashboard.component.html
│   │   │   ├── dashboard.component.ts
│   │   │   ├── dashboard.module.ts
│   │   │   └── dashboard.routing.ts
│   │   ├── forms
│   │   │   ├── equal-validator.directive.ts
│   │   │   ├── extendedforms
│   │   │   │   ├── extendedforms.component.html
│   │   │   │   └── extendedforms.component.ts
│   │   │   ├── forms.module.ts
│   │   │   ├── forms.routing.ts
│   │   │   ├── regularforms
│   │   │   │   ├── regularforms.component.html
│   │   │   │   └── regularforms.component.ts
│   │   │   ├── validationforms
│   │   │   │   ├── password-validator.component.ts
│   │   │   │   ├── validationforms.component.html
│   │   │   │   └── validationforms.component.ts
│   │   │   └── wizard
│   │   │       ├── wizard.component.html
│   │   │       └── wizard.component.ts
│   │   ├── layouts
│   │   │   ├── admin
│   │   │   │   ├── admin-layout.component.html
│   │   │   │   └── admin-layout.component.ts
│   │   │   └── auth
│   │   │       ├── auth-layout.component.html
│   │   │       └── auth-layout.component.ts
│   │   ├── maps
│   │   │   ├── fullscreenmap
│   │   │   │   ├── fullscreenmap.component.html
│   │   │   │   └── fullscreenmap.component.ts
│   │   │   ├── googlemaps
│   │   │   │   ├── googlemaps.component.html
│   │   │   │   └── googlemaps.component.ts
│   │   │   ├── maps.module.ts
│   │   │   ├── maps.routing.ts
│   │   │   └── vectormaps
│   │   │       ├── vectormaps.component.html
│   │   │       └── vectormaps.component.ts
│   │   ├── pages
│   │   │   ├── lock
│   │   │   │   ├── lock.component.html
│   │   │   │   └── lock.component.ts
│   │   │   ├── login
│   │   │   │   ├── login.component.html
│   │   │   │   └── login.component.ts
│   │   │   ├── pages.module.ts
│   │   │   ├── pages.routing.ts
│   │   │   └── register
│   │   │       ├── register.component.html
│   │   │       └── register.component.ts
│   │   ├── shared
│   │   │   ├── fixedplugin
│   │   │   │   ├── fixedplugin.component.html
│   │   │   │   ├── fixedplugin.component.ts
│   │   │   │   └── fixedplugin.module.ts
│   │   │   ├── footer
│   │   │   │   ├── footer.component.html
│   │   │   │   ├── footer.component.ts
│   │   │   │   └── footer.module.ts
│   │   │   └── navbar
│   │   │       ├── navbar.component.html
│   │   │       ├── navbar.component.ts
│   │   │       └── navbar.module.ts
│   │   ├── sidebar
│   │   │   ├── sidebar.component.html
│   │   │   ├── sidebar.component.ts
│   │   │   └── sidebar.module.ts
│   │   ├── tables
│   │   │   ├── datatable.net
│   │   │   │   ├── datatable.component.html
│   │   │   │   └── datatable.component.ts
│   │   │   ├── extendedtable
│   │   │   │   ├── extendedtable.component.html
│   │   │   │   └── extendedtable.component.ts
│   │   │   ├── regulartable
│   │   │   │   ├── regulartable.component.html
│   │   │   │   └── regulartable.component.ts
│   │   │   ├── tables.module.ts
│   │   │   └── tables.routing.ts
│   │   ├── timeline
│   │   │   ├── timeline.component.html
│   │   │   ├── timeline.component.ts
│   │   │   ├── timeline.module.ts
│   │   │   └── timeline.routing.ts
│   │   ├── userpage
│   │   │   ├── user.component.html
│   │   │   ├── user.component.ts
│   │   │   ├── user.module.ts
│   │   │   └── user.routing.ts
│   │   └── widgets
│   │       ├── widgets.component.html
│   │       ├── widgets.component.spec.ts
│   │       ├── widgets.component.ts
│   │       ├── widgets.module.ts
│   │       └── widgets.routing.ts
│   ├── assets
│   │   ├── css
│   │   ├── fonts
│   │   ├── img
│   │   ├── js
│   │   └── scss
│   │       ├── paper-dashboard
│   │       └── paper-dashboard.scss
│   ├── environments
│   ├── favicon.ico
│   ├── index.html
│   ├── main.ts
│   ├── polyfills.ts
│   ├── test.ts
│   ├── tsconfig.app.json
│   ├── tsconfig.spec.json
│   └── typings.d.ts
├── tsconfig.json
├── tslint.json
├── typings
└── typings.json

```

## Browser Support

At present, we officially aim to support the last two versions of the following browsers:

<img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/chrome.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/firefox.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/edge.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/safari.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/opera.png" width="64" height="64">

## Licensing

- Copyright 2018 Creative Tim (https://www.creative-tim.com)
- Creative Tim [license](https://www.creative-tim.com/license)
